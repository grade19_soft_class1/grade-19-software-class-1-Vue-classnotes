## 土狗碎碎念专栏...😶

### <font color="#00dd00" face="comic sans ms,sans-serif">一、Vue之模板语法</font>

#### <mark>1.1 插值(数据绑定语法)</mark>

* <font color="#16a085" face="comic sans ms,sans-serif">文本---使用 v-text 指令/ {{XXX}}</font>

```vue
<span v-text="msg"></span>
<!-- 和下面的一样 -->
<span>{{msg}}</span>
```
 

* <font color="#16a085" face="comic sans ms,sans-serif">原始HTML---使用 v-html 指令：</font>

```vue
    <div id="app">
        <H3 v-html="content"></H3> <!-- 直接渲染出来 -->
        <H3>{{content}}</H3>
    </div>

    <script src="../../js/vue.js"></script>
    <script>
        let vm =new Vue({
            el:'#app',
            data:{
                content:'<h3>小明想喝奶</h3>'
            }
        })
    </script>
```

> ✍ 使用 v-html 会直接把标签自带样式或者添加的style渲染出来
> PS: 你的站点上动态渲染的任意 HTML 可能会非常危险，因为它很**容易导致 XSS 攻击**。  
> &emsp;&nbsp;&nbsp;  请只对可信内容使用 HTML 插值，⭐绝不要对用户提供的内容使用插值。

 
* <font color="#16a085" face="comic sans ms,sans-serif">Attribute绑定---使用 v-bind指令 添加属性</font>

```vue
    <div id="app">
        <button v-bind:disabled="isButtonDisabled" @click="handle">我是一个按钮,只为能被点</button>
    </div>
 
    <script src="../../js/vue.js"></script>
    <script>
        let vm =new Vue({
            el:'#app',
            data:{
                isButtonDisabled:true //默认
                // isButtonDisabled:false
                // isButtonDisabled:false
                // isButtonDisabled:null
     
            },
            methods:{
                handle:function(){
                    console.log('被点了被点了(●ˇ∀ˇ●)');
                }
            }
        })
    </script>
```



* <font color="#16a085" face="comic sans ms,sans-serif">使用JavaScript表达式</font>

  * 表达式：由变量函数返回值和运算符以及常量组成的式子

  ```vue
  {{ number + 1 }}
  {{ ok ? 'YES' : 'NO' }}
  {{ message.split('').reverse().join('') }}
  <div v-bind:id="'list-' + id"></div>
  ```

#### <mark>1.2 指令</mark>

* 什么是指令？

  * 指令的本质就是自定义属性
  * 指令的格式：以v-开始（比如： v-cloak）

* 参数

  * 一些指令能够接收一个“参数”，在指令名称之后以冒号表示。
  * 比如：`<a v-bind:href="url">...</a> `

* 动态参数

  ```vue
  <div id="app">
          <a v-bind:[someAttr]="url">我是谁?我在哪?我要去哪里?🐌</a>
      </div>
  
      <script src="../../js/vue.js"></script>
      <script>
          let vm =new Vue({
              el:'#app',
              data:{
                  someattr:'href',
                  url:'https://www.baidu.com'
              }
          })
      </script>
  ```

  > 📝 在 DOM 中使用模板时 (直接在一个 HTML 文件里撰写模板)，  还需要避免使用大写字符来命名键名，因为浏览器会把 attribute 名全部强制转为小写。
  >
  > 如果转换成小写，无法跟Vue实例中的data属性里面的属性名称对应，就会报错如下异常：
  >
  > <font color="#e74c3c" face="comic sans ms,sans-serif">Invalid value for dynamic directive argument (expected string or null): </font>

* 修饰符

  * 修饰符 (modifier) 是以半角句号 `.` 指明的特殊后缀，用于指出一个指令应该以特殊方式绑定。

  * 例如，`.prevent` 修饰符告诉 `v-on` 指令对于触发的事件调用 `event.preventDefault()`：

    ```Vue 
    <form v-on:submit.prevent="onSubmit">...</form> // 阻止默认事件
    ```


##### v-show 和 v-if 指令的共同点和不同点? 

* 相同点:v-show 和 v-if 都能控制元素的显示和隐藏。

* 不同点:
  * 实现本质方法不同
    * v-show 本质就是通过设置 css 中的 **display 设置为 none**,控制隐藏
    * v-if 是动态的向 **DOM 树**内添加或者删除 DOM 元素

  * 编译的区别
    * v-show其实就是在控制css
    * v-if切换有一个局部编译/卸载的过程，切换过程中合适地销毁和重建内部的事件监听和子组件
  * 编译的条件
    * v-show都会编译，初始值为false，只是将display设为none，但它也编译了
    * v-if初始值为false，就不会编译了
 
#### <mark>1.3 缩写(非常人性化)</mark>

* v-bind 缩写

  ```vue
  <!-- 完整语法 -->
  <a v-bind:href="url">...</a>
  
  <!-- 缩写 -->
  <a :href="url">...</a>
  
  <!-- 动态参数的缩写 (2.6.0+) -->
  <a :[key]="url"> ... </a>
  ```

* v-on 缩写

  ```vue
  <!-- 完整语法 -->
  <a v-on:click="doSomething">...</a>
  
  <!-- 缩写 -->
  <a @click="doSomething">...</a>
  
  <!-- 动态参数的缩写 (2.6.0+) -->
  <a @[event]="doSomething"> ... </a>
  ```


### <font color="#00dd00" face="comic sans ms,sans-serif">二、Vue之计算属性与侦听器</font>

#### <mark>2.1 计算属性 computed</mark>

* 对于任何复杂逻辑，你都应当使用**计算属性**

  ```vue
   <div id="app">
          <p>Original msg:"{{msg}}"</p>
          <p>Computed msg:"{{reversedMsg}}"</p>
      </div>
  
      <script src="../../js/vue.js"></script>
      <script>
        let vm =new Vue({
            el:'#app',
            data:{
                msg:'上海自来水'
            },
            computed:{
                reversedMsg:function(){
                    return this.msg.split('').reverse().join('')
                }
            }
        })
      </script>
  ```

  >  在表达式中调用方法来达到同样的效果：不同的是**计算属性是基于它们的响应式依赖进行缓存的**。

  ```vue
  <p>Reversed message: "{{ reversedMessage() }}"</p>
  // 在组件中
  methods: {
    reversedMessage: function () {
      return this.message.split('').reverse().join('')
    }
  }
  ```

 
#### <mark>2.2 侦听器 watch</mark>

> 虽然计算属性在大多数情况下更合适，但有时也需要一个自定义的侦听器。
>
> 这就是为什么 Vue 通过 `watch` 选项提供了一个更通用的方法，来响应数据的变化。
>
> 当需要在数据变化时执行异步或开销较大的操作时，这个方式是最有用的。
 