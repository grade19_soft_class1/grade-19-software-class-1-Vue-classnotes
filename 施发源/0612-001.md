# 06-12笔记

## 单文件组件
+ 老规矩先创建vue cli,不会可以查看之前的毕竟喔喔

+ 好闸门开始了，先创建一个项目就叫vue-app吧
```
vue create vue-app
```

+ 创建完成后我们使用vs code打开安装Vetur扩展这样就有代码高亮语法提示

### 最最简单的单文件组件

+ 接下来我们开始创建一个简单的单文件组件
+ 找到src/components文件夹在这里新建一个vue组件文件
```
<template>
  <div>
    <h2>我是新来的</h2>
    <h2>{{name}}</h2>
    <h2>{{age}}</h2>
    <ul>
        <li v-for="i in aihaos" :key="i.id">{{i.aihao}}</li>
    </ul>
  </div>
</template>

<script>
export default {
    props:{
        age:{
            type:String,
            default:'我是默认年龄'
        },
        aihaos:{
            type:Array,
            default:function(){
                return [
                    {
                        id:1,
                        aihao:'睡觉'
                    },
                    {
                        id:2,
                        aihao:'吃饭'
                    },
                    {
                        id:3,
                        aihao:'蹦迪'
                    },
                ]   

            }
        }
    },
    data:function(){
        return{
            name:'小鱼鱼'
        }
    }
}
</script>

<style>
    h2{
        size: 50px;
        color: green;
    }
</style>
```
+ 创建完要在app.vue中使用
```
<template>
  <div id="app">
    <HelloWorld msg="你好"/>
    <myapp :aihaos='wowo' />
    <myapp :age='19' />
  </div>
</template>

<script>
import HelloWorld from './components/HelloWorld.vue'
import Myapp from './components/myapp.vue'

export default {
  name: 'App',
  components: {
    HelloWorld,
    Myapp
  },
  data:function(){
    return {
      wowo:[
        {
          id:1,
          aihao:'运动'
        },
        {
          id:2,
          aihao:'干饭'
        },
        {
          id:3,
          aihao:'游戏'
        },
        
      ]
    }
  }
}
</script>
<style>
#app {
  font-family: Avenir, Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

* 终于折腾出来了！嘻嘻嘻