# 重定向和别名
[重定向和别名的区别说明](https://blog.csdn.net/buduoduoorg/article/details/108486792)
# 路由组件传参
### 使用router-link传参
```
    <router-link :to="{name:'content',query:{name:123}}" >content</router-link>

    <router-link :to="{name:'content',params:{name:123}}" >content</router-link>

    <router-link :to="{path:'/content',query:{name:123}}" >content</router-link>

    使用path路径跳转：传参必须使用query,使用命名视图跳转params和query都可以
    使用params：如果路由路径上没有参数接收，刷新页面数据会丢失，使用query会把参数显示在地址栏路径上，此时刷新数据不会丢失
```
### 使用编程式导航传参
```
    普通传参
    var param={name:{name:"瓜皮",age:"22",grade:"幼稚园大班"},age:12}

    this.$router.push({name:"ChildContent",params:param})

    this.$router.push({name:"ChildContent",query:param})

    this.$router.push({path:"/ChildContent",params:param})
    
    使用path路径跳转，传参必须使用query,使用命名视图跳转params和query都可以
```
### 动态路由传参
```
    route.js
    {
        path: '/ChildContent/:id/post/:name',
        component: ChildContent,
        name:"ChildContent",
        props:true  //此时params就是组件的props
    }
    执行跳转的组件
    var param={name:{name:"瓜兮兮",age:"22",grade:"幼稚园小班"},age:12}
    this.$router.push({path:"ChildContent/123/post/456",query:param}) 
    跳转到的组件
    mounted(){
            console.log(this.$route);
            console.log(this.id,this.name);
        },
    props:["name","id","params"]
    此时刷新params数据不会丢失，因为在路由路径上有对应的数据，比方上面的id和name
    在router对象中有props选项，会让传递的params参数当做跳转到页面props中的参数，不需要通过this.$route获取，有解耦的作用
```

## HTML5 History模式
[Apache+HTML5History模式](https://blog.csdn.net/guojun13598079464/article/details/81357569?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-5&spm=1001.2101.3001.4242)

### history.pushState
```
    history.pushState(state,title,url);
    一、二参数可以为空，第三个参数表示新历史记录的地址，浏览器在调用pushState()方法后不会去加载这个URL,新的URL不一定要是绝对地址，如果它是相对的，它一定是相对于当前的URL.
```
### history.replaceState
```
    history.replaceState(state,title,url);
    window.history.replaceState和window.history.pushState类似，不同之处在于replaceState不会在window.history里新增历史记录点，其效果类似于window.location.replace(url),都是不会在历史记录点里新增一个记录点的
```
### window.onpopstate
```
    监听url的变化
    window.addEventListener("popstate", function() {
        var currentState = history.state;
        /*
        * 触发事件后要执行的程序
        */                                            
    });

    //或者
    window.onpopstate = function(){}

    javascript脚本执行window.history.pushState和window.history.replaceState不会触发onpopstate事件，在浏览器点击前进或者后退会触发

    谷歌浏览器和火狐浏览器在页面第一次打开的反应是不同的，谷歌浏览器奇怪的是回触发onpopstate事件，而火狐浏览器则不会
```
### 后端nginx配置
```
    location / {
        try_files $uri $uri/ /index.html;
    }
    1>按顺序检查文件是否存在，返回第一个找到的文件.
    2>若没有文件，查找uri/目录。结尾的斜线表示为文件夹uri/.
    3>如果所有的文件都找不到，会进行一个内部重定向到最后一个参数.
    4>务必确认只有最后一个参数可以引起一个内部重定向，之前的参数只设置内部URI的指向.
    最后一个参数是回退URI且必须存在，否则将会出现内部500错误.
```