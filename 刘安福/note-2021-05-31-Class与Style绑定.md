# 五月三十一是结束也是开始
## 听说这周都有雨但是我希望不要下雨
----------

# 绑定 HTML Class

## 对象语法
```
v-bind:class 指令也可以与普通的 class attribute 共存。

v-bind:style 的对象语法十分直观——看着非常像 CSS，但其实是一个 JavaScript 对象。CSS property 名可以用驼峰式 (camelCase) 或短横线分隔 (kebab-case，记得用引号括起来) 来命名
```

# 其它
```
    background-color 这样的形式在对象中会报错
    所以 我们可以写成：
        'background-color'或者
         backgroundColor 



```
# 代码
```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>wawawa</title>
</head>

<body >
    <form id="app" >
        <input type="button" value="点我变色" @click="updateColor">
        <div :style="styleObj">
            <h1 >yi二yi</h1>
        </div>
    </form>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        //随机颜色 A-F 0-9  #ffd8d9
        let word = ['A', 'B', 'C', 'D', 'E', 'F', 1, 2, 3, 4, 5, 6, 7, 8, 9,0];
        let fullColor = "#";

        for (var i = 1; i <= 6; i++) {
            let radWord = Math.floor(Math.random() * word.length)//随机取得Word中的一个元素索引
            let currentColor = word[radWord];//随机取得A-F 1-9
            fullColor+=currentColor;//拼接颜色
        }
        console.log(fullColor);
        let aColor="#"+fullColor.slice(1,7).split('').reverse().join('')

        console.log(aColor);

        let vm = new Vue({
            el: "#app",
            data: {
                updateColor:function(){
                    window.location.href="index.html";//刷新网页
                }
            },
            computed: {
                styleObj: function () {
                    return {
                        color: aColor,
                        'background-color':fullColor
                    }
                }
            }
        })
    </script>
</body>
</html>
```