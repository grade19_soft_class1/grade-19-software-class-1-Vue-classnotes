# 二〇二一年六月二十一日 漂泊大雨

## 利用Vscode进行 dotnet项目的开发
```
    最开始可以去扩展商店中安装一些C#的扩展（下载量多的基本都可以安装）
    EXtensions  XML
```
### 通过终端的一些dotnet命令来创建我们的Api项目
```
    创建项目：创建一个文件夹（切勿使用中文文件夹） 利用vscode打开
        一、
            dotnet new sln :创建项目解决方案    
                出现：The template "Solution File" was created successfully.就创建好了一个以你的文件夹命名的解决方案
        二、
            1.mkdir firstDotnetProject.Api 创建一个以自己文件命名的文件夹
            2.cd firstDotnetProject.Api  进入到刚刚创建的文件夹 

        三、
            1.dotnet new webapi --no-https ： 创建一个不带https的项目

            1.dotnet new webapi :创建webapi服务 （不推荐）
                此时生成的是一个以自己文件夹命名的一些项目 并且有带https的项目
        亖、
            1.dotnet run ：运行项目 
            打开监听的本地地址的端口 访问控制器（Controllers）中的WeatherForecastController.cs中的 [HttpGet]的WeatherForecast 此时网页上才会有显示虚拟且随机的天气

            2.cd .. ：返回上一个文件夹
        伍、
            1.dotnet sln add firstDotnetProject.Api 将自己创建的项目的工程文件添加到解决方案中去
            2.dotnet sln list : 列出解决方案文件中的所有项目。(前提是在项目解决方案的文件夹下)
        陆、
            1.mkdir firstDotnetProject.Data :创建一个以自己项目问文件名的Data文件夹
            2. cd firstDotnetProject.Data :进入文件夹
        柒、
            1.dotnet new classlib : 添加类库
            2.dotnet add package Newtonsoft.json :向项目文件添加包引用。
            3.dotnet sln add firstDotnetProject.Data ：将自己创建的项目的工程文件添加到解决方案中去

            3.cd .. :回到上级目录
        捌、
            1.dotnet add firstDotnetProject.Api reference firstDotnetProject.Data :添加引用 向项目添加项目到项目引用
            2.dotnet new classlib -n firstDotnetProject.Domain : 添加一个名叫 firstDotnetProject.Domain的类库
            3.dotnet sln add firstDotnetProject.Domain ：将自己创建的项目的工程文件添加到解决方案中去
            3.修改 .Api文件夹中的.Api.csprj文件 多添加一行
                <ProjectReference Include="..\firstDotnetProjcet.Domain\firstDotnetProjcet.Domain.csproj" />
                你会发现这行和上一行只有Domain之差
        玖、此时使用VsStudio打开项目解决方案时 就会显示三个 因为" dotnet sln add "了三次 所以有三个
            
```
```

    一些特殊命令 ：
        dotnet -h : 显示命令行帮助。其实就是help 查看现在命令下所有的可使用的命令 使用方式 可以有 dotnet -h 或者 dotnet new -h 
```
# 一键生成项目

### 复制下面的代码，修改文件名xxxx.bat结尾 XXX就是你项目名了 注意保存的盘符 会按照你当前盘符在当前盘生成项目 （别使用中文）
```
:: 设置批处理文件所在目录为解决方案所在目录
cd /d %~dp0

:: 显示一些提示信息
@echo 请修改批处理文件名（以批处理的文件名作为解决方案的名称）
@pause
 
set name=%~n0
 
 ::创建解决方案的目录
mkdir %name%
cd %name%



:: 创建类库目录，进入类库目录，并使用dotnet命令创建类库项目（默认使用项目文件夹名称作为项目名称）
mkdir %name%.Data
cd %name%.Data
dotnet new classlib
dotnet add package log4net
dotnet add package Newtonsoft.Json
cd ..
 
mkdir %name%.Domain
cd %name%.Domain
dotnet new classlib
dotnet add package log4net
dotnet add package Newtonsoft.Json
cd ..
 
mkdir %name%.Api
cd %name%.Api
dotnet new webapi
dotnet add package log4net
dotnet add package Newtonsoft.Json
cd ..
dotnet add %name%.Api/%name%.Api.csproj reference %name%.Data/%name%.Data.csproj
dotnet add %name%.Api/%name%.Api.csproj reference %name%.Domain/%name%.Domain.csproj

::sln
dotnet new sln

 
dotnet sln %name%.sln add %name%.Api/%name%.Api.csproj --solution-folder 01Web
dotnet sln %name%.sln add %name%.Data/%name%.Data.csproj --solution-folder 02Business
dotnet sln %name%.sln add %name%.Domain/%name%.Domain.csproj --solution-folder 02Business

 
::编译解决方案
dotnet build
 
::end
 
@echo 构建完成，按任意键退出
@pause
```