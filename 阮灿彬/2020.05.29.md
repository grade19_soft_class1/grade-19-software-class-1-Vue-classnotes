# 模板语法
### 数据绑定最常见的形式就是使用“Mustache”语法 (双大括号) 的文本插值:<span>Message: {{ msg }}</span> Mustache 标签将会被替代为对应数据对象上 msg property 的值。无论何时，绑定的数据对象上 msg property 发生了改变，插值处的内容都会更新。通过使用 v-once 指令，你也能执行一次性地插值，当数据改变时，插值处的内容不会更新。但请留心这会影响到该节点上的其它数据绑定：
```
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<body>
  <div id="app" v-once>{{message}}</div>
  <div id="app-1">{{message}}</div>
  <script>
    //如果显示的信息后续不需要再修改，使用v-once
    //使用了v-once之后，在控制台上修改app.message无法更改
    //v-once用于一次性数据，固定数据
    const app = new Vue({
      el: '#app',
      data: {
        message: 'test',
      }
    })
    var app2 = new Vue({

      el: '#app-1',
      data: {
        message: '123'
      }
    })
  </script>

</body>
```
![](./imgs/2021-05-30_2.png)
![](./imgs/2021-05-30_3.png)
### 双大括号会将数据解释为普通文本，而非 HTML 代码。为了输出真正的 HTML，你需要使用 v-html
```
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<body> 
<div id="app">

  <span>{{message}}</span>
<span v-html="message"></span>
</div>

<script>
  var app = new Vue({
    el:'#app',
    data:{
      message:'<h1>这就是示例</h1>'
    }
  })
</script>
</body>
```
![](./imgs/2021-05-30_1.png)
### Attribute
```
对于布尔 attribute (它们只要存在就意味着值为 true)

<script src="https://unpkg.com/vue/dist/vue.js"></script>

<body>
<div id="app">
<button v-bind:disabled="isButtonDisabled">按钮</button>
</div>
<script>
  var app = new Vue({
    el:'#app',
    data:{
      isButtonDisabled:true
    }
  })
</script>
</body>
```
![](./imgs/2021-05-31_1.png)
![](./imgs/2021-05-31_2.png)
## 参数
```
一些指令能够接收一个参数，在指令名称之后以冒号表示。例如，v-bind 指令可以用于响应式地更新 HTML attribute，并通过“url”这个参数来控制a标签的不同链接。
<body>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <div id="app">
    <div><a v-bind:href="url"> 这是一个链接</a></div>
  </div>
  <script>
    var vm = new Vue({
      el: '#app',
      data: {
        url: 'https://www.baidu.com/'
      }
    })
  </script>
</body>
这就是一个指令中的简单参数，它能够在既有的功能上做出我们所需的各种改变，但是却无法做功能上的变化，就如上面所说v-bind绑定的href只能作为a标签的链接，为了解决这个问题，我们可以使用动态参数。
```
### 动态参数
```
<body>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
<div id="app">
  <a v-bind:[something]="url">这是一个链接</a>
</div>
<script>
  var vm = new Vue({
    el:'#app',
    data:{
      url:'https://www.baidu.com',
      something:'href'
    }
  })
</script>
</body>
对动态参数的值的约束
动态参数预期会求出一个字符串，异常情况下值为 null。这个特殊的 null 值可以被显性地用于移除绑定。任何其它非字符串类型的值都将会触发一个警告。

对动态参数表达式的约束
动态参数表达式有一些语法约束，因为某些字符，如空格和引号，放在 HTML attribute 名里是无效的。例如：

<!-- 这会触发一个编译警告 -->
<a v-bind:['foo' + bar]="value"> ... </a>
变通的办法是使用没有空格或引号的表达式，或用计算属性替代这种复杂表达式。

在 DOM 中使用模板时 (直接在一个 HTML 文件里撰写模板)，还需要避免使用大写字符来命名键名，因为浏览器会把 attribute 名全部强制转为小写：

<!--
在 DOM 中使用模板时这段代码会被转换为 `v-bind:[someattr]`。
除非在实例中有一个名为“someattr”的 property，否则代码不会工作。
-->
<a v-bind:[someAttr]="value"> ... </a>
```
## 修饰符
```
事件修饰符
在事件处理程序中调用 event.preventDefault() 或 event.stopPropagation()是非常常见的需求。尽管我们可以在 methods 中轻松实现这点，但更好的方式是：methods 只有纯粹的数据逻辑，而不是去处理 DOM 事件细节。

为了解决这个问题， Vue.js 为 v-on 提供了 事件修饰符。通过由点(.)表示的指令后缀来调用修饰符。

.stop
.prevent
.capture
.self
.once
复制代码
<!-- 阻止单击事件冒泡 -->
<a v-on:click.stop="doThis"></a>
 
<!-- 提交事件不再重载页面 -->
<form v-on:submit.prevent="onSubmit"></form>
 
<!-- 修饰符可以串联 -->
<a v-on:click.stop.prevent="doThat"></a>
 
<!-- 只有修饰符 -->
<form v-on:submit.prevent></form>
 
<!-- 添加事件侦听器时使用事件捕获模式 -->
<div v-on:click.capture="doThis">...</div>
 
<!-- 只当事件在该元素本身（而不是子元素）触发时触发回调 -->
<div v-on:click.self="doThat">...</div>
```
## 计算机属性
```
模板内的表达式非常便利，但是设计它们的初衷是用于简单运算的。在模板中放入太多的逻辑会让模板过重且难以维护。例如：

<div id="example">
  {{ message.split('').reverse().join('') }}
</div>



<div id="example">
  <p>Original message: "{{ message }}"</p>
  <p>Computed reversed message: "{{ reversedMessage }}"</p>
</div>
var vm = new Vue({
  el: '#example',
  data: {
    message: 'Hello'
  },
  computed: {
    // 计算属性的 getter
    reversedMessage: function () {
      // `this` 指向 vm 实例
      return this.message.split('').reverse().join('')
    }
  }
})
结果：

Original message: "Hello"

Computed reversed message: "olleH"
```
## 计算属性的setter
```
在 Vue 中，computed 的属性可以被视为是 data 一样，可以读取和设值，因此在 computed 中可以分成 getter（读取） 和 setter（设值），一般情况下是没有 setter 的，computed 预设只有 getter ，也就是只能读取，不能改变设值。
 computed: {
    fullName: function () {
      return this.firstName + ' ' + this.lastName
    }
  }
})
其实computed里的代码完整的写法应该是：

 computed: {
    fullName: {
      get(){
         return this.firstName + ' ' + this.lastName
      }
    }
  }
```