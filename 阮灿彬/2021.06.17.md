### 伪元素
```
<template>
  <div class="hello">
    <ul>
      <li>apple</li>
      <li>banana</li>
      <li>watermelon</li>
    </ul>
  </div>
</template>

<script>
export default {
  name: 'HelloWorld',
  props: {
    msg: String
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
li::marker{
  content:">>";
  color:red;
}
li:hover::marker{
  content: "》》";
  color: green;
}
</style>
```
![](./imgs/2021-06-17_1.png)
## 动态路由匹配
```
静态路由是不可以传递参数的。需要传递参数得用到动态路由
在参数名前面加上 : ，然后将参数写在路由的 path 内 
router里的index.js

import Cry from '../views/Cry.vue'
routes: [

    //将页面组件与path指令的路由关联

   { name: 'BookInfo', path: '/books/:id', component: BookInfo}

]
这样定义之后，vue-router就会匹配所有的：/books/1,/books/2,/books/3 ……，所以说这样定义的路由的数量是不确定的。
```
### 在views里的Cru.vue
```
<template>
  <div>
      图书ID:{{$route.params.id}}
  </div>
</template>
```
### App.vue
```
        <li>
          <router-link :to ="{name:'BookInfo',params :{id:1}}" >
            <div>首页</div>
          </router-link>
        </li>
```
### 效果
![](./imgs/2021-06-18_1.jpg)
![](./imgs/2021-06-18_2.jpg)