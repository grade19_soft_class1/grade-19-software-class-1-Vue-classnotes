# 实例生命周期钩子
每个 Vue 实例在被创建时都要经过一系列的初始化过程——例如，需要设置数据监听、编译模板、将实例挂载到 DOM 并在数据变化时更新 DOM 等。同时在这个过程中也会运行一些叫做生命周期钩子的函数，这给了用户在不同阶段添加自己的代码的机会。

![](./img/Life.png)
***

- beforeCreate:在一个实例被创建前执行代码。
- created:在一个实例被创建后执行代码。
- beforeMount:在节点挂载前执行代码。
- mounted:在节点挂载完成时执行代码。
- beforeUpdate:在数据更新前执行代码。
- updated:在数据更新完成时执行代码。
- beforeDestroy:实例销毁前执行代码。
- destroy:实例销毁后执行代码。

## 常用的几个属性
- el属性 ：用来指示vue编译器从什么地方开始解析 vue的语法，可以说是一个占位符。
- data属性：用来组织从view中抽象出来的属性，可以说将视图的数据抽象出来存放在data中。
- template属性：用来设置模板，会替换页面元素，包括占位符。
- methods属性：放置页面中的业务逻辑，js方法一般都放置在methods中
- render属性：创建真正的Virtual Dom
- computed属性：用来计算属性
- watch属性：watch:function(new,old){}，监听data中数据的变化，两个参数，一个返回新值，一个返回旧值