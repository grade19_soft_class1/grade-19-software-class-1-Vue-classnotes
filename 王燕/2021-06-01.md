## 今天雨真的超级大，全身都湿了，都能拧出水来(￣_￣|||)
#  列表渲染
## 1 . 1 用 v-for 把一个数组对应为一组元素
用 v-for 指令基于一个数组来渲染一个列表。v-for 指令需要使用 item in items 形式的特殊语法，其中 items 是源数据数组，而 item 则是被迭代的数组元素的别名。
```
    <div id="app">
        <ul>
            <li v-for="item in items":key="item.msg " >
                {{item.msg}}
            </li>
        </ul>
     
    </div>
```
```
let vm=new Vue({
            el:"#app",
            data:{
                items:[
                    {
                        msg:'57'
                    },
                    {
                        msg:'24'
                    },
                    {
                        msg:'45'
                    },
                    {
                        msg:'56'
                    }
                ]
            }
        })
```
结果：

![](./imgs/18.png)

加上前缀的话：
```
    <div id="app">
        <ul>
            <li v-for="(item,key) in items" >
                {{key+1}}-{{item.msg}}
            </li>
        </ul>
     
    </div>
```
结果：

![](./imgs/19.png)

也可以用 of 替代 in 作为分隔符
```
<div v-for="item of items"></div>
```
## 1 . 2 在 v-for 里使用对象
```
   <div id="app">
        <ul>
            <li v-for="(item,key) in items">
                {{key}}-{{item}}
            </li>
        </ul>
    </div>
```
```
        let vm = new Vue({
            el: "#app",
            data: {
                items: {
                    title: '唧唧复唧唧',
                    author: '木兰辞',
                    time: '2022-09-12'
                }
            }
        })
```
结果：

![](./imgs/20.png)

还可以用多个参数
```
 <ul>
            <li v-for="(item,key,index) in items">
                {{index}}.{{key}}-{{item}}
            </li>
 </ul>
```
![](./imgs/21.png)

### 在遍历对象时，会按 <code>items.keys()</code> 的结果遍历，但是不能保证它的结果在不同的 JavaScript 引擎下都一致。
## 1 . 3 变更方法
+ push()

+ pop()

+ shift()

+ unshift()

+ splice()

+ sort()

+ reverse()
## 1 . 4 在 v-for 里使用值范围
```
<div>
  <span v-for="n in 10">{{ n }} </span>
</div>
```
结果：

        12345678910
## 1 . 5 在组件上使用 v-for
```
<div id="app">
        <form action="">
            <input type="text" v-model ="newitem">
            <input type="button" value="添加" @click="addtodo">
        </form>
        <ul>
            <li v-for="item in todos">{{item.txt}}</li>
        </ul>
    </div>
```
```
       let vm = new Vue({
            el: "#app",
            data: {
                newitem:'',
                todos: [
                    {
                        id: 1,
                        txt: 'mother'
                    },
                    {
                        id: 2,
                        txt: 'father'
                    },
                    {
                        id: 3,
                        txt: 'sister'
                    },
                    {
                        id: 4,
                        txt: 'bother'
                    },
                ]
            },
            methods:{
                   addtodo:function(){
                       console.log(this.newitem);
                       this.todos.push({
                           id:this.todos.length,
                           txt:this.newitem
                       })
                   }
            }
        
        })
```
结果：

![](./imgs/22.png)

### 还可以添加或移除

添加：
```
<div id="app">
        <form action="">
            <input type="text" v-model ="newitem">
            <input type="button" value="添加" @click="addtodo">
        </form>
        <ul>
            <li v-for="item in todos">{{item.txt}}</li>
        </ul>
    </div>
```

![](./imgs/23.png)

移除：

html:
```
  <ul>
            <do-item v-for="(item,index) in todos":key="item.id":title="item.txt" @remove="todos.splice(index)"></do-item>
        </ul>
```
js:
```
 Vue.component('do-item',{
            props:['title'],
            template:`<li>{{title}}<input type="button" value="移除" @click="$emit('remove')"></li>`
        }
```
![](./imgs/24.png)