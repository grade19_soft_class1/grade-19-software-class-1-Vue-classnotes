# 表单输入绑定
# 一 、 基础用法
```
用 v-model 指令在表单 <input>、<textarea> 及 <select> 元素上创建双向数据绑定。它会根据控件类型自动选取正确的方法来更新元素。尽管有些神奇，但 v-model 本质上不过是语法糖。它负责监听用户的输入事件以更新数据，并对一些极端场景进行一些特殊处理。
```
### v-model 会忽略所有表单元素的 value、checked、selected attribute 的初始值而总是将 Vue 实例的数据作为数据来源。你应该通过 JavaScript 在组件的 data 选项中声明初始值。
v-model 在内部为不同的输入元素使用不同的 property 并抛出不同的事件：

+ text 和 textarea 元素使用 value property 和 input 事件；

+ checkbox 和 radio 使用 checked property 和 change 事件；

+ select 字段将 value 作为 property 并将 change 作为事件。

对于需要使用输入法 (如中文、日文、韩文等) 的语言，你会发现 v-model 不会在输入法组合文字过程中得到更新。如果你也想处理这个过程，请使用 input 事件:
```
    <div id="app">
    <input type="text" v-model="chi" @input="abc">
    <label for="">{{chi}}</label>
    </div>
```
```
       let vm = new Vue({
            el: "#app",
            data: {
                chi:'',
                lists:[
                    {

                    }
                ]
            },
            methods:{
                abc:function(){
                    console.log(this.chi);
                }
            }
        })
```
![](./imgs/27.png)
## 1 . 1 文本
```
   <div id="app">
    <input type="text" v-model="chi">
    <label for="">{{chi}}</label>
    </div>
```
![](./imgs/28.png)
## 1 . 2 多行文本
```
   <div id="app">
    <textarea v-model="chi" ></textarea>
    <p >{{ chi }}</p>
    </div>
```
![](./imgs/29.png)
```
### 在文本区不能插入( <textarea>{{text}}</textarea> )不然不能生效，应该用  v-model  代替
```
## 1 . 3 复选框
```
    <div id="app">
    <input type="checkbox" v-model="checked" @change="abc">哈喽
    </div>
```
```
       let vm = new Vue({
            el: "#app",
            data: {
                checked:true,
                lists:[
                    {

                    }
                ]
            },
            methods:{
                abc:function(){
                    console.log(this.checked);
                }
            }
        })
```
![](./imgs/30.png)
### 多个复选框
```
   <div id="app">
    <input type="checkbox" v-model="checked" @change="abc" value="1">哈喽
    <input type="checkbox" v-model="checked" @change="abc" value="2">芜湖
    <input type="checkbox" v-model="checked" @change="abc" value="3">呀哈
    <input type="checkbox" v-model="checked" @change="abc" value="4">哦吼
    </div>
```
```
        let vm = new Vue({
            el: "#app",
            data: {
                checked:[],
                lists:[
                    {

                    }
                ]
            },
            methods:{
                abc:function(){
                    console.log(this.checked);
                }
            }
        })
```
![](./imgs/31.png)
这是绑定为数组
## 1 . 4 单选按钮
```
   <div id="app">
    <input type="radio" v-model="checked" @change="abc" value="1">哈喽
    <input type="radio" v-model="checked" @change="abc" value="2">芜湖
    </div>
```
```
        let vm = new Vue({
            el: "#app",
            data: {
                checked:[],
                lists:[
                    {

                    }
                ]
            },
            methods:{
                abc:function(){
                    console.log(this.checked);
                }
            }
        })
```
![](./imgs/32.png)
## 1 . 5 选择框
单选时：
```
   <div id="app">
        <select name="" id="" v-model="selected" @change="selectChange">
            <option disabled value="">请选择...</option>
            <option value="1">A</option>
            <option value="2">B</option>
            <option value="3">C</option>
            <option value="4">D</option>
            <option value="5">E</option>
            <option value="6">F</option>
        </select>
        <span>Selected: {{ selected }}</span>
    </div>
```
```
       let vm = new Vue({
            el: "#app",
            data: {
                selected:'',
                lists:[
                    {

                    }
                ]
            },
            methods:{
                selectChange:function(){
                    console.log(this.selected);
                }
            }
        })
```
![](./imgs/33.png)

多选时(绑定到一个数组)：
```
   <div id="app">
        <select name="" id="" v-model="selected" @change="selectChange" multiple style="width: 50px;">
            <option disabled value="">请选择...</option>
            <option value="1">A</option>
            <option value="2">B</option>
            <option value="3">C</option>
            <option value="4">D</option>
            <option value="5">E</option>
            <option value="6">F</option>
        </select>
        <span>Selected: {{ selected }}</span>
    </div>
```
![](./imgs/34.png)

这时要按住Ctrl键才能多选，不然只能单选
# 二 、 值绑定
对于单选按钮，复选框及选择框的选项，、<code> v-model </code> 绑定的值通常是静态字符串 (对于复选框也可以是布尔值)：
```
<!-- 当选中时，`picked` 为字符串 "a" -->
<input type="radio" v-model="picked" value="a">

<!-- `toggle` 为 true 或 false -->
<input type="checkbox" v-model="toggle">

<!-- 当选中第一个选项时，`selected` 为字符串 "abc" -->
<select v-model="selected">
  <option value="abc">ABC</option>
</select>
```
## 2 . 1 复选框
```
<input
  type="checkbox"
  v-model="toggle"
  true-value="yes"
  false-value="no"
>
```
```
// 当选中时
vm.toggle === 'yes'
// 当没有选中时
vm.toggle === 'no'
```
## 2 . 2 单选按钮
```
<input type="radio" v-model="pick" v-bind:value="a">
```
```
// 当选中时
vm.pick === vm.a
```
## 2 . 3 选择框的选项
```
<select v-model="selected">
    <!-- 内联对象字面量 -->
  <option v-bind:value="{ number: 123 }">123</option>
</select>
```
```
// 当选中时
typeof vm.selected // => 'object'
vm.selected.number // => 123
```
# 三 、修饰符
## 3 . 1 .lazy
```
<input v-model.lazy="msg">
```
## 3 . 2 .number
```
<input v-model.number="age" type="number">
```
## 3 . 3 .trim
```
<input v-model.trim="msg">
```