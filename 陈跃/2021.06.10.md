## 用yarn安装vue cli之后显示vue NotFound
> 解决方法：改系统的环境变量给Path新增一个路径

## 在阿里云上部署创建完的vue项目
    1.现在阿里云上解析一个域名
![](./img/域名.png)

    2.在/var/www目录上创建一个跟你域名一样的文件夹
![](./img/2.png)

    3.修改权限，使得任何人在此目录下拥有最高权限
![](./img/3.png)

    4.把创建好的vue打包好放入/var/www/vue.chenyyy.top文件夹中
![](./img/5.png)

    5.在/etc/nginx/conf.d目录下写配置文件
![](./img/4.png)

    6.配置文件
![](./img/6.png)

    7.写完配置文件，输入nginx-t查看配置文件有没有配置成功,成功后重启nginx -s reload
![](./img/7.png)
![](./img/7.1.png)

    8.效果
![](./img/8.png)