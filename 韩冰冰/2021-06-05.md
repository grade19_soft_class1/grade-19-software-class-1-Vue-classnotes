# 表单的输入绑定
## 基础用法
可以用v-model指令在表单、及元素上创建双向数据绑定。它会根据控件类型自动选取正确的方法来更新元素。它会负责监听用户的输入事件以更新数据，并对一些极端场景进行一些特殊处理。

v-model会忽略所有表单元素的value、checked、selected特性的初始值而总是将Vue实例的数据作为数据来源。你应该通过JavaScript在组件的data选项中声明初始值。

v-model早内部为不同的输入元素使用不同的property并抛出不同的事件：
* text和textarea元素使用value property和input事件；
* checkbox和radio使用checked property和change事件；
* select字段将value作为prop并将change作为事件。

对于需要使用输入法 (如中文、日文、韩文等) 的语言，你会发现 v-model 不会在输入法组合文字过程中得到更新。如果你也想处理这个过程，请使用 input 事件。
### 文本
```
<body>
    <div id="app">
        <input type="text" v-model="tryatry" @input="afn">
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        let app = new Vue({
            el:'#app',
            data:{
                tryatry:'',
                lists:[
                    {

                    }
                ]
            },
            methods:{
                afn:function(){
                    console.log(this.tryatry);
                }
            }
        })
    </script>
</body>
```
### 多行文本
```
<textarea name="" id="" cols="30" rows="10"></textarea>
```
在文本区域插值```(<textarea>{{text}}</textarea>)```并不会生效，应用v-model来代替。
### 复选框
```
<body>
    <div id="app">
        <input type="checkbox" v-model="checked" @change="checkboxChange" value="1">这个会是什么？
        <input type="checkbox" v-model="checked" @change="checkboxChange" value="2">你怎么了
        <input type="checkbox" v-model="checked" @change="checkboxChange" value="3">没事吧

        <input type="text" v-model="test" value="123456">
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        let app = new Vue({
            el:'#app',
            data:{
                checked:[],
                tryatry:'',
                lists:[
                    {

                    }
                ],
                test:'4569'
            },
            methods:{
                checkboxChange:function(){
                    console.log(this.checked);
                }
            }
        })
    </script>
</body>
```
![图片无法显示](./imgs/2021-06-05_103726.png)

![图片无法显示](./imgs/2021-06-05_103824.png)
### 单选按钮
```
<body>
    <div id="app">
        <input type="radio" v-model="singleChecked" @change="radioChange" value="1">男
        <input type="radio" v-model="singleChecked" value="0">女
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        let app = new Vue({
            el:'#app',
            data:{
                singleChecked:'',
                lists:[
                    {

                    }
                ]
            },
            methods:{
                radioChange:function(){
                    console.log(this.singleChecked);
                }
            }
        })
    </script>
</body>
```
![图片无法显示](./imgs/2021-06-05_104328.png)
### 选择框
```
<body>
    <div id="app">
        <select name="" id="" multiple v-model="selected" @change="selectChange">
            <option disabled value="">请您选择一个</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
        </select>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        let app = new Vue({
            el:'#app',
            data:{
                selected:[],
                lists:[
                    {

                    }
                ]
            },
            methods:{
                selectChange:function(){
                    console.log(this.selected);
                }
            }
        })
    </script>
</body>
```
![图片无法显示](./imgs/2021-06-05_110126.png)

![图片无法显示](./imgs/2021-06-05_110143.png)

如果 v-model 表达式的初始值未能匹配任何选项，```<select>``` 元素将被渲染为“未选中”状态。在 iOS 中，这会使用户无法选择第一个选项。因为这样的情况下，iOS 不会触发 change 事件。因此，更推荐像上面这样提供一个值为空的禁用选项。
## 绑定值
对于单选按钮，复选框及选择框的选项，v-model绑定的值通常是静态字符串(对于复选框也可以是布尔值)
### 复选框
这里的true-value和false-value attribute并不会影响输入控件的value attribute，因为浏览器在提交表单时并不会包含未被选中的复选框。如果要确保表单中这两个值中的一个能够被提交，(即“yes”或“no”)，请换用单选按钮。
### 单选按钮
```
<body>
    <div id="app">
        <input type="radio" v-model.number="radios" v-bind:value="wu" @change="radioChange" type="number">这里有一个单选按钮
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        let app = new Vue({
            el:'#app',
            data:{
                radios:'',
                wu:'水獭',
                lists:[
                    {

                    }
                ]
            },
            methods:{
                radioChange:function(){
                    console.log(this.radios);
                }
            }
        })
    </script>
</body>
```
![图片无法显示](./imgs/2021-06-05_111601.png)
### 选择框的选项
关于select选项的写法有三种情况：
1. 写在HTML中，
2. 写在JS一个数组中，
3. 通过接口是option的值写在HTML
## 修饰符
* .lazy
在默认情况下，v-model在每次input事件触发将输入框的值与数据进行同步。你可以添加lazy修饰符，从而转变为使用change事件进行同步。
```
<input v-model.lazy="msg">
```
* .number
如果想要自己将用户的输入值转为数值类型，可以给v-model添加number修饰符
```
<input v-model.number="age" type="number">
```
这通常很有用，因为即使在 type="number" 时，HTML 输入元素的值也总会返回字符串。如果这个值无法被 parseFloat() 解析，则会返回原始的值。
* .trim
如果要自动过滤用户输入的首尾空白字符，可以给v-model添加trim修饰符
```
<input v-model.trim="msg">
```