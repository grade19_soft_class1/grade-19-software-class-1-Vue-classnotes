# 今日份课堂笔记
## 模板语法
### 插值
#### 文本
数据绑定最常见的形式就是使用"Mustache"语法的文本插值
```
    <div id="app">
        <span>{{content}}</span>
        <span v-html="content"></span>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    
    <script>
        let app=new Vue({
            el:'#app',
            data:{
                content:'<h1>难度--等什么君</h1>'
            }
        })
    </script>
```
注意：

你的站点上动态渲染的任意 HTML 可能会非常危险，因为它很容易导致 XSS 攻击。请只对可信内容使用 HTML 插值，绝不要对用户提供的内容使用插值。
#### Attribute
Mustache 语法不能作用在 HTML attribute 上，遇到这种情况应该使用 `v-bind` 指令
```
<body>
    <div id="app">
        <span>{{content}}</span>
        <span v-html="content"></span>
        <button v-ind:disabled="isButtonDisabled">一个小按钮</button>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    
    <script>
        let app=new Vue({
            el:'#app',
            data:{
                content:'<h1>难度--等什么君</h1>',
                isButtonDisabled:true
            }
        })
    </script>
</body>
```

如果 `isButtonDisabled` 的值是 `null`、`undefined `或` false`，则 `disabled attribute` 甚至不会被包含在渲染出来的 `<button>` 元素中。
### 指令
1. 什么是指令？

指令的本质就是自定义属性

指令的格式:以v-开始

2. v-clock指令用法

* 插值表达式存在的问题：“闪动”。即当不断刷新页面时，可能会出现闪动的现象，就是先出现{{msg}}插值表达式，之后才会迅速替换为具体显示的内容

* 如何解决该问题：使用v-cloak指令

* 解决该问题的原理：先隐藏，替换好值之后再显示最终的值
3. MVVM设计思想

* M(model)：提供数据
* V(view)：提供页面展示效果
* VM(View-Model)：实现控制逻辑
* view与model不能直接交互，必须要有中介View-Model

4. v-if与v-show的区别

* v-if控制元素是否渲染到页面
* v-show控制元素是否显示（已经渲染到了页面），类似于display:none
* 如果我们需要频繁的显示和隐藏某个元素，那就使用v-show
* 如果我们希望元素渲染之后基本上变化比较少了，那就用v-if
#### 参数
```
<body>
    <div id="app">
        <span v-bind:[attrName]="url">海底</span>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    
    <script>
        let app=new Vue({
            el:'#app',
            data:{
                content:'<h1>难度--等什么君</h1>',
                isButtonDisabled:true,
                seen:true,
                url:'http://baidu.com',
                attrname:'href'
            }
        })
    </script>
</body>
```
### 缩写
#### v-bind缩写
```
<!-- 完整语法 -->
<a v-bind:href="url">...</a>

<!-- 缩写 -->
<a :href="url">...</a>

<!-- 动态参数的缩写 (2.6.0+) -->
<a :[key]="url"> ... </a>
```
#### v-on缩写
```
<!-- 完整语法 -->
<a v-on:click="doSomething">...</a>

<!-- 缩写 -->
<a @click="doSomething">...</a>

<!-- 动态参数的缩写 (2.6.0+) -->
<a @[event]="doSomething"> ... </a>
```