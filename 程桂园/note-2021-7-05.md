# 今日份课堂笔记
## JWT
### 什么是JWT
JSON Web Token（JWT）是目前最流行的跨域身份验证解决方案。

通俗地来讲，JWT是能代表用户身份的令牌，可以使用JWT令牌在api接口中校验用户的身份以确认用户是否有访问api的权限。

JWT中包含了身份认证必须的参数以及用户自定义的参数，JWT可以使用秘密（使用HMAC算法）或使用RSA或ECDSA的公钥/私钥对进行签名。

JWT官网地址：http://jwt.io

### 什么时候能使用JSON Web令牌
1. 授权：这是使用JWT的最常见方案。一旦用户登录，每个后续请求将包括JWT，允许用户访问该令牌允许的路由，服务和资源。Single Sign On是一种现在广泛使用JWT的功能，因为它的开销很小，并且能够在不同的域中轻松使用。

2. 信息交换：JSON Web令牌是在各方之间安全传输信息的好方法。此外，由于使用标头和有效负载计算签名，您还可以验证内容是否未被篡改。

### 传统的身份验证方式
传统身份验证方式：

    * 用户向服务器发送用户名和密码。

    * 服务器验证通过后，在当前对话（session）里面保存相关数据，比如用户角色、登录时间等等。
    
    * 服务器向用户返回一个 session_id，写入用户的 Cookie。

    * 用户随后的每一次请求，都会通过 Cookie，将 session_id 传回服务器。

    * 服务器收到 session_id，找到前期保存的数据，由此得知用户的身份。

### Payload 有效载荷
Payload 部分也是一个 JSON 对象，用来存放实际需要传递的数据。JWT 规定了7个官方字段，供选用。

```
iss (issuer)：签发人

exp (expiration time)：过期时间

sub (subject)：主题

aud (audience)：受众

nbf (Not Before)：生效时间

iat (Issued At)：签发时间

jti (JWT ID)：编号
```