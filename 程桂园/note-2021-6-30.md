# 今日份补课笔记
## IQueryable
### 简介
LINQ（Language Integrated Query）即“语言集成查询”，于2007年作为 .NET Framework 3.5的一部分加入到框架中。LINQ的加入给予了.NET无与伦比的简便的方式来查询及处理各种数据源对应的“集合”。
### 主要类型及基本原理
1. IQueryable

如果说接口的含义就是定义了类型的某种“能力”，那么执行LINQ操作所需的能力就是由接口IQueryable来定义的，所以实现IQueryable接口的类就可以执行LINQ操作。

IQueryable同时也继承了IEnumerable，所以实现IQueryable的类型也必定是一个可枚举类型。

2. Queryable

IQueryable只定义了执行LINQ所需的“能力”，像where，select这些方法则是由Queryable实现的，Queryable是一个静态类，内部基本都是IQueryable接口的扩展方法，这些扩展方法利用了IQueryable接口提供的“能力”来实现LINQ功能。

Queryable所做的事情相对比较简单，具体的工作还是由Expression和IQueryProvider来实现的。

### 使用说明

链式方法查询中会使用到的方法
```
Aggregate，为集合的元素应用一个累加器，你可以指定累加器的实现。

All，判断是否集合中所有元素都满足给定的条件。

Any，判断是否集合中存在一个元素满足给定条件或者是否存在元素。

Append，在集合的尾部添加元素。

AsQueryable，将一个IEnumerable转化为IQueryable（对应的实现类型为EnumerableQuery）。

Average，计算一个数值集合的平均值。

Cast，将集合中的元素转化为指定的类型。

Concat，连接两个集合。

Contains，判断集合是否包含指定的元素。

Count，计算集合中元素的数量。

DefaultIfEmpty，获取集合，但如果集合是空的话返回包含一个默认元素的集合。

Distinct，返回元素均为唯一的集合。

ElementAt，获取指定索引（Index）处的元素。

ElementAtOrDefault，获取指定索引（Index）处的元素，若元素为空则返回默认值。

Except，获取排除指定元素后的集合。

First，获取集合中的第一个元素。

FirstOrDefault，获取集合中的第一个元素，若为空则返回默认值。

GroupBy，使用指定的条件对集合进行分组，使用指定方式构建新元素并返回新的集合。

GroupJoin，分组与关联两个存在“主外键”关系的集合。

Intersect，对比指定集合获取交叉项，可指定交叉项的对比方法。

Join，使用匹配的键值关联指定的集合。

Last，获取集合中的最后一个元素

LastOrDefault，获取集合中的最后一个元素，若为空则返回默认值。

LongCount，以Int64来返回集合中元素的数量。

Max，获取指定属性值为最大的元素。

Min，获取指定属性值为最小的元素。

OfType，使用指定的类型过滤集合中的元素。

OrderBy，对集合进行升序排序，可以指定排序的属性。

OrderByDescending，对集合进行降序排序，可以指定排序的属性。

Prepend，在集合的头部添加元素。

Reverse，翻转集合顺序。

Select，将集合中的每个元素转为你指定的新的格式。

SelectMany，若集合中的元素存在子集合，则可通过该方法将这些属性“选择”出来
并生成一个新的集合。

SequenceEqual，判断是否与指定的集合相同。

Single，获取集合中唯一一个元素，若集合元素为空或超过一个将抛出异常。

SingleOrDefault，获取结合中唯一一个元素，若集合元素为空或超过一个则获取默认值。

Skip，跳过指定数量的元素，并返回剩余元素的集合。

SkipLast，从尾部开始跳过指定数量元素，并返回剩余元素集合。

SkipWhile，跳过指定条件的元素，并返回剩余元素集合。

Sum，计算集合中指定属性值得总和。

Take，从头部开始获取指定个数的元素。

TakeLast，从尾部开始获取指定个数的元素。

TakeWhile，获取满足指定条件的元素的集合。

ThenBy，以升序对集合进行二次排序。

ThenByDescending，以降序对集合进行二次排序。

Union，合并两个集合。

Where，指定的条件过滤集合。

Zip，使用指定的方式结合两个集合。
```