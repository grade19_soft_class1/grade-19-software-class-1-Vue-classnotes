# 今日份课堂笔记
## Vue CLI
### 安装
1. 打开Windows PowerShell

2. 淘宝镜像
```
npm config set registry http://registry.npm.taobao.org
```
3. npm i -g yarn

4. yarn global add @vue/cli

5. 编辑环境变量

![](./imgs/1.png)

![](./imgs/2.png)

6. vue

7. d:

8. mkdir myApp

9. cd .\myApp\

9. vue create my-vue-app

    选择  Default ([Vue 2] babel, eslint)

    选择  Use Yarn

10. cd my-vue-app

11. yarn serve

12. 打开创建的 my-vue-app 文件

打开终端
```
$ yarn build
```