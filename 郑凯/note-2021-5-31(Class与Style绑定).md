# 前言

### 对象语法
+ 入门练习
动态地切换 class
```
    <style>
        [v-cloak]{
            display: none;
        }
        .backColor{
            color: blue;
        }
    </style>

</head>
<body>
    <div id="app">
        <p v-cloak>
            <font :class="{backColor:isblue,redColor:isred}">
                文案
            </font>
        </p>
    </div>


<script src="./vue.js"></script>
<script>
    let Vm=new Vue({
        el:"#app",
        data:{
            isblue:true,
            isred:false
```
+ 绑定的数据对象可以不必内联定义在模板里：
```
</head>
<body>
    <div id="app">
        <p v-cloak>
            <font :class="ClassObject">
                文案
            </font>
        </p>
    </div>


<script src="./vue.js"></script>
<script>
    let Vm=new Vue({
        el:"#app",
        data:{
            
            ClassObject:{backColor:true,redColor:false}
        }
    })
</script>
```
+ 绑定一个返回对象的计算属性
```
<body>
    <div id="app">
        <p v-cloak>
            <font :class="ClassObject">
                文案
            </font>
            <font :class="classObject01">
                text
            </font>
        </p>
    </div>


<script src="./vue.js"></script>
<script>
    let Vm=new Vue({
        el:"#app",
        data:{
            ClassObject:{backColor:true,redColor:false}
        },
        computed:{
            classObject01:function(){
                
                return {
                    
                    backColor:true,
                    redColor:false
                }
            }
        }
    })
</script>
```
### 数组语法
> 建立一个class列表，并且引用
+ 文字变色
```
<body>
    <div id="app">
        <p v-cloak>
            <font :style="styleObject"><!--这里的class要改成style-->
                文案
            </font>      
        </p>
    </div>
<script src="./vue.js"></script>
<script>
    let Vm=new Vue({
        el:"#app",
        computed:{
            styleObject:function(){
                let arr=['red','blue','gray','black','gold'];
                let rnd=Math.floor(Math.random()*arr.length)
                return{
                    color:arr[rnd]
                }

            }
        }
    })
</script>
```
+ 添加文字背景颜色框
```
<body>
    <div id="app">
        <p v-cloak>
            <font :style="styleObject"><!--这里的class要改成style-->
                文案
            </font>      
        </p>
    </div>
<script src="./vue.js"></script>
<script>
    let Vm=new Vue({
        el:"#app",
        computed:{
            styleObject:function(){
                let arr=['red','blue','gray','black','gold'];
                let rnd=Math.floor(Math.random()*arr.length)
                let rmd=Math.floor(Math.random()*arr.length)
                //添加判断，相同则重新加载，这里的if可以换成while（百分百不重复）
                if(rnd===rmd){
                    rmd=Math.floor(Math.random()*arr.length)
                }
                return{
                    color:arr[rnd],
                    'background-color':arr[rmd]
                }

            }
        }
    })
</script>
```
### 布局
（https://www.cnblogs.com/coco1s/p/14816871.html）
+ 瀑布流布局:pinterest.com
