
## v-model
v-model.lazy在你在输入框输入时不进行同步，只有在你输入完后按下回车等操作后才会进行同步   
v-model.trim去除输入框中首尾的空格   
v-model.mumber将输入框中的值转化成number类型，若无法转化则保持原数据
```
    <div id="app">
        <input type="text" v-model.lazy="value"><hr>
        <div>{{value}}</div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        let app = new Vue({
        el:'#app',
        data:{
            value:''
        }
    })
    </script>
```
你可以用 v-model 指令在表单 \<input>、\<textarea> 及 \<select> 元素上创建双向数据绑定。   
### v-model 在内部为不同的输入元素使用不同的 property 并抛出不同的事件：   
1. text 和 textarea 元素使用 value property 和 input 事件；对input进行任意操作都会触发input事件
```
 <div id="app">
        <input type="text" v-model="value" @input="lazyinput"><hr>
        <div>{{value}}</div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        let app = new Vue({
        el:'#app',
        data:{
            value:''
        },
        methods:{
            lazyinput:function(){
                console.log(this.value);
            }
        }
    });
    </script>
```
2. checkbox 和 radio 使用 checked property 和 change 事件；当checkbox的值发生改变时会触发change事件   
```
 <div id="app">
        <input type="checkbox" v-model="value" @change="lazyinput"><hr>
        <div>{{value}}</div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        let app = new Vue({
        el:'#app',
        data:{
            value:false
        },
        methods:{
            lazyinput:function(){
                console.log(this.value);
            }
        }
    });
    </script>
```
3. select 字段将 value 作为 prop 并将 change 作为事件。当选择的值发生改变时会触发change事件   
```
<div id="app">
       <select name="" id="" v-model="value" @change="lazyinput">
           <option disabled="disabled" value="--请选择--">--请选择--</option>
           <option value="1">1</option>
           <option value="2">2</option>
           <option value="3">3</option>
           <option value="4">4</option>
           <option value="5">5</option>
       </select>
        <div>{{value}}</div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>

        let app = new Vue({
        el:'#app',
        data:{
            value:'--请选择--'
        },
        methods:{
            lazyinput:function(){
                console.log(this.value);
            }
        }
    });
    </script>
```