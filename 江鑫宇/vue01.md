# Vue.js 是什么?
Vue 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。另一方面，当与现代化的工具链以及各种支持类库结合使用时，Vue 也完全能够为复杂的单页应用提供驱动。  

尝试 Vue.js 最简单的方法是使用 Hello World 例子。你可以在浏览器新标签页中打开它，跟着例子学习一些基础用法。或者你也可以创建一个 .html 文件，然后通过如下方式引入 Vue：   
 开发环境版本，包含了有帮助的命令行警告:   
\<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js">\</script>   
或者：
 生产环境版本，优化了尺寸和速度:   
\<script src="https://cdn.jsdelivr.net/npm/vue">\</script>

## 声明式渲染
Vue.js 的核心是一个允许采用简洁的模板语法来声明式地将数据渲染进 DOM 的系统：
```
<div id="app">
  {{ message }}
</div>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
})
输出结果为：Hello Vue！
```
除了文本插值，我们还可以像这样来绑定元素 attribute：
```
<div id="app-2">
  <span v-bind:title="message">
    鼠标悬停几秒钟查看此处动态绑定的提示信息！
  </span>
</div>


<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
var app2 = new Vue({
  el: '#app-2',
  data: {
    message: '页面加载于 ' + new Date().toLocaleString()
  }
})

```

v-bind attribute 被称为指令。指令带有前缀 v-，以表示它们是 Vue 提供的特殊 attribute。