# 处理用户输入
v-on 指令添加一个事件监听器，通过它调用在 Vue 实例中定义的方法：
```
<div id="app-5">
  <p>{{ message }}</p>
  <button v-on:click="reverseMessage">反转消息</button>
</div>


var app5 = new Vue({
  el: '#app-5',
  data: {
    message: 'Hello Vue.js!'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})
```
 v-model 指令，它能轻松实现表单输入和应用状态之间的双向绑定。
 ```
 <div id="app-6">
  <p>{{ message }}</p>
  <input v-model="message">
</div>


var app6 = new Vue({
  el: '#app-6',
  data: {
    message: 'Hello Vue!'
  }
})
 ```