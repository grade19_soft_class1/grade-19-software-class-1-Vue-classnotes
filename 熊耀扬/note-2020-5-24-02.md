# 常用指令
指令带有前缀 v-，以表示它们是 Vue 提供的特殊 attribute。

## v-html
```
<body>
    
    <div id="app">
        <p>{{msg}}</p>
        <p v-html="msg">{{msg}}</p>
    </div>
    <script src="./vue.js"></script>
    <script>
        var app = new Vue({
            el:'#app',
            data:{
                msg:"<h3>msg</h3>"
            }
        })
    </script>
</body>
```
效果懂的都懂

## v-text
```
<body>
    <div id = "app">
        <div v-text = "name"></div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                name:"<h1>Hello World</h1>"
            }
        })
    </script>
</body>
```
![图片裂了！！！](imgs/03.png)

## v-for
```
<body>
    <div id = "app">
        <h3>灿彬的爱好</h3>
        <ul>
            <li v-for = "item in ruan">{{item}}</li>
        </ul>
        <h3>他们的爱好</h3>
        <ul>
            <li v-for = "his in they">{{his.name}}的爱好是{{his.hobby}}</li>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                ruan:["吃饭","睡觉","打豆豆"],
                they:[
                    {
                        name:"ruan",
                        hobby:"吃饭"
                    },
                    {
                        name:"jing",
                        hobby:"美女"
                    },
                    {
                        name:"hao",
                        hobby:"电影"
                    }
                ]
            }
        })
    </script>
</body>
```
![图片裂了！！！](imgs/04.png)

## v-if
```
<body>
    <div id = "app">
        <div v-if = "role == 'ruan'">
            <h3>叼毛</h3>
        </div>
        <div v-else-if = "role == 'jing'">
            <h3>景儿</h3>
        </div>
        <div v-else><h3>无</h3></div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                role:"hao"
            }
        })
    </script>
</body>
```
当 data 里 role 是 ruan 时，网页显示 叼毛 ，role 是 jing 时显示 景儿，其它则显示 无

## v-show
```
<body>
    <div id = "app">
        <div v-if = "isShow">
            <h3>叼毛</h3>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                isShow:true
            }
        })
    </script>
</body>
```
效果懂的都懂

## v-if 和 v-show 的性能比较
实现方式：v-if底层采用的是appendChild来实现的，v-show通过样式的display控制标签的显示，正因为实现方式上面有差异，导致了他们的加载速度方面产生了差异；

加载性能：v-if加载速度更快，v-show加载速度慢

切换开销：v-if切换开销大，v-show切换开销小

v-if是惰性的，它是“真正”的条件渲染，因为它会确保在切换过程中条件块内的事件监听器和子组件适当地被销毁和重建，v-show 也是惰性的：如果在初始渲染时条件为假，则什么也不做——直到条件第一次变为真时，才会开始渲染条件块。

v-show 就简单得多——不管初始条件是什么，元素总是会被渲染，并且只是简单地基于 CSS 进行切换。

一般来说，v-if有更高的切换开销，而v-show有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用v-show较好，如果在运行时条件很少改变，则使用v-if较好。

## v-bind
```
<body>
    <div id = "app">
        <a v-bind:href="link" v-bind:class="{active:isActive}">我的微说说</a>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                link:"https://yang.lx15149.com",
                isActive:true
            }
        })
    </script>
</body>
```
点击我的微说说即可跳到我的微说说界面

## v-on(使用v-on我们可以在标签上面绑定事件)
```
<body>
    <div id = "app">
        <!-- 绑定一个事件 -->
        <a 
        v-bind:href="link"
        v-bind:class="{active:isActive}"
        v-on:click="myClick"
        >我的微说说</a>
        <!-- 绑定多个事件 -->
        <button v-on="{
            click:myClick,
            mouseenter:mouseEnter,
            mouseleave:mouseLeave
        }">点下试试</button>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                link:"https://yang.lx15149.com",
                isActive:false
            },
            methods:{
                myClick:function(){
                    console.log("你还真点")
                },
                mouseEnter:function(){
                    console.log("鼠标在这")
                },
                mouseLeave:function(){
                    console.log("鼠标呢")
                }
            }
        })
    </script>
</body>
```
按下F12，当鼠标碰到点下试试时，后端打印 鼠标在这，鼠标离开时打印 鼠标呢，点击时打印 你还真点

## v-model
我们需要vue实例可以帮我们渲染数据并响应式的监听数据修改，同时我们还需要监听用户行为，如果用户在标签上面修改了数据（之前的修改，指的是通过vue实例app01进行的数据修改），我们需要获取到数据，针对这个需求，我们可以使用v-model指令。
```
<body>
    <div id = "app">
        <br />
        <p><input type="text" v-model = "name"></p>
        <p>请选择你的性别</p>
        <p>
            <input type="checkbox" value="男" v-model="gender"/>
            <input type="checkbox" value="女" v-model="gender"/>
        </p>
        <br />
        {{name}}
        {{gender}}

        <p>请选择你的专业</p>
        <select name="" id="" v-model="major">
            <option>软件技术</option>
            <option>计算机应用技术</option>
            <option>工商管理</option>
            <option>电子商务</option>
        </select>
        <br />
        {{major}}

        <p>
            <textarea v-model = "text"></textarea>
        </p>
        {{text}}
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                name:"ruan",
                gender:[],
                major:[],
                text:"这是一个文本"
            }
        })
    </script>
</body>
```
![图片裂了！！！](imgs/05.png)

文本框的内容可在网页更改

多选时:

```
<body>
    
    <div id="app">
        <select name="" id="" multiple v-model="se">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
        </select>
        <p>{{se}}</p>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>
        let app = new Vue({
            el:'#app',
            data:{
                se:[]
            }
        })
    </script>
</body>
```

用 v-for 渲染：
```
<body>
    
    <div id="app">
        <select name="" id="" multiple v-model="se">
            <option v-for="item in items" :value="item.value">{{item.id}}</option>
        </select>
        <p>{{se}}</p>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>
        let app = new Vue({
            el:'#app',
            data:{
                se:[],
                items:[
                    {id:1,value:'A'},
                    {id:2,value:'B'},
                    {id:3,value:'C'},
                    {id:4,value:'D'},
                    {id:5,value:'E'},
                    {id:6,value:'F'},
                    ]
            }
        })
    </script>
</body>
```

## 指令修饰符
以此可以限定用户输入的数据类型
```
<body>
    <div id = "app">
        <table border="1">
                <tr>
                    <td>
                        <th>学科</th>
                        <th>成绩</th>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Java</td>
                    <td><input type="text" v-model.number="Java"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>HTML</td>
                    <td><input type="text" v-model.lazy="Css"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Sql</td>
                    <td><input type="text" v-model.trim="Sql"></td>
                </tr>
        </table>
        {{Java}}
        {{Css}}
        {{SQL}}
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                Java:78,
                Css:86,
                Sql:88
            },
            methods:{
                show(){
                    console.log(this.Java)
                    console.log(this.Css)
                    console.log(this.Sql)
                }
            }
        })
    </script>
</body>
```
![图片裂了！！！](imgs/06.png)

v-model.lazy 是当你改变值时，鼠标光标还在框内，内容不会立即刷新，当你光标移到外面时并点击才会改变，也就是说，当你加上 .lazy后，双向数据绑定就不起作用了,剩下两个都不用多说了，应该很好理解。

## 计算属性
计算属性用来监听多个数据，每次页面加载，计算属性中的函数立即执行，但是只要原数据不被修改，那么，就不会触发重新计算，计算属性会使用计算后的缓存结果，只当原数据修改时，才会重新计算并将结果缓存起来。计算属性的计算结果可以当做data中的数据一样使用。
```
<body>
    <div id = "app">
        <table border="1">
                <tr>
                    <td>
                        <th>学科</th>
                        <th>成绩</th>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>Chinese</td>
                    <td><input type="text" v-model.number="Chinese"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>English</td>
                    <td><input type="text" v-model.lazy="English"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Math</td>
                    <td><input type="text" v-model.lazy="Math"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>总分</td>
                    <td>{{Chinese + English + Math}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>平均分</td>
                    <td>{{avgScore}}</td>
                </tr>
        </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                Chinese:78,
                English:86,
                Math:66
            },
            computed:{
                sumScore:function(){
                    return this.Chinese + this.English + this.Math;
                },
                avgScore:function(){
                    return this.sumScore/3;
                }
            }
        })
    </script>
</body>
```
![图片裂了！！！](imgs/07.png)

如果要改数据请不要在网页修改，看看下图你就知道了

![图片裂了！！！](imgs/08.png)

## 侦听属性
侦听属性Watch，当你有一些数据需要随着其它数据变动而变动时，就可以使用Watch来监听他们之间的变化。

```
<body>
    
    <div id="app">
        {{fullName}}
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>

        let app = new Vue({
            el:'#app',
            data:{
                firstName:"first",
                lastName:"last",
                fullName:'first last'
            },
            watch:{
                // firstName:function(val){
                //     this.fullName = val + ' ' + this.lastName
                // },
                // lastName:function(val){
                //     this.fullName = this.firstName + ' ' + val
                // }
                fullName:function(val){
                    this.fullName = this.firstName + ' ' + this.lastName
                }
            }
        })
    </script>
</body>
```
## 计算属性版本
```
<body>
    
    <div id="app">
        {{fullName}}
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>

        let app = new Vue({
            el:'#app',
            data:{
                firstName:"first",
                lastName:"last"
            },
            computed:{
                fullName:function(val){
                    return this.firstName + ' ' + this.lastName
                }
            }
        })
    </script>
</body>
```

这样确实比上面的侦听的要好一些

## 自定义指令

当我们需要对普通DOM元素进行底层操作时，就需要用到自定义指令


### 聚焦输入框

```
<body>
    
    <div id="app">
        <input type="text" v-foucs>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>


        //全局注册
        Vue.directive('focus',{
            inserted:function(el){
                el.focus();//聚焦
            }
        })

        let app = new Vue({
            el:'#app',
        })
    </script>
</body>
```
### 先写到这吧，后续待写