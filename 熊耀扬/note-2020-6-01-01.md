# 列表渲染

## 用 v-for 把一个数组对应为一组数组
```
<body>

    <div id="app">
        <ul>
            <li v-for="item in items">
                {{item.message}}
            </li>
        </ul>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        
        let app = new Vue({
            el:'#app',
            data:{
                items:[
                    {message:'春'},
                    {message:'夏'},
                    {message:'秋'},
                    {message:'冬'}
                ]
            }
        })
    </script>
</body>
```

结果
```
· 春
· 夏
· 秋
· 冬
```

## 索引参数
```
<body>

    <div id="app">
        <ul>
            <li v-for="(item,index) in items">
               {{index+1}} - {{item.message}}
            </li>
        </ul>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        
        let app = new Vue({
            el:'#app',
            data:{
                items:[
                    {message:'春'},
                    {message:'夏'},
                    {message:'秋'},
                    {message:'冬'}
                ]
            }
        })
    </script>
</body>
```

结果

```
· 1 - 春
· 2 - 夏
· 3 - 秋
· 4 - 冬
```


## 在 v-for 里使用对象
```
<body>

    <div id="app">
        <ul>
            <li v-for="(item,name) in object">
               {{name}} : {{item}}
            </li>
        </ul>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        
        let app = new Vue({
            el:'#app',
            data:{
                object:{
                    name:'张三',
                    age:20,
                    height:'174cm',
                    weight:'57kg'
                }
            }
        })
    </script>
</body>
```

结果

```
· name : 张三
· age : 20
· height : 174cm
· weight : 57kg
```

## Template循环渲染多标签

```
<body>
    <div id="app">
        <ul>
            <template v-for="item in items">
                <li>{{item.msg}}</li>
                <li>bbb</li>
                <li>ccc</li>
            </template>
        </ul>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        
        let app = new Vue({
            el:'#app',
            data:{
                items:[
                    {msg:'aaa'}
                ]
            }
        })
    </script>
</body>
```

结果

```
· aaa
· bbb
· ccc
```

## v-for 数组对应的更新

由于Vue的机制就是检测数据的变化，自动跟新HTML。数组的变化，Vue之检测部分函数，检测的函数执行时才会触发视图更新。这些方法如下：
```
· push()
· pop()
· shift()
· unshift()
· splice()
· sort()
· reverse()
```


## 一个例子，每3秒添加一条数据
```
<body>
    <div id="app">
        <table>
            <thead>
                <tr>
                    <th>姓名</th>
                    <th>年龄</th>
                    <th>性别</th>
                </tr>
            </thead>
            <tbody v-if = "UserList.length > 0">
                <tr v-for = "item in UserList">
                    <td>{{item.name}}</td>
                    <td>{{item.age}}</td>
                    <td>{{item.sex}}</td>
                </tr>
            </tbody>
            <tbody v-else>
                <tr><td colspan="3">无数据</td></tr>
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        
        let app = new Vue({
            el:'#app',
            data:{
                UserList:[
                    {name:'李四',age:20,sex:'男'}
                ]
            }
        });
        setInterval(function(){
            app.UserList.push({'name':'张三','age':'20','sex':'男'});
        },3000)
    </script>
</body>
```

## 二个例子,简单对数据进行增加和移除
```
<body>
    
    <div id="app">
        <form action="">
            <input type="text" v-model="newList" id="text">
            <input type="button" value="添加" @click="addNewList">
            <!-- <input type="button" value="添加" @click="addNewList"> -->
        </form>

        <ul>
            <do-list 
                v-for="(item,index) in lists" 
                :key="item.id" 
                :title="item.todoList" 
                @remove="lists.splice(index,1)">
            </do-list>
        </ul>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>

        Vue.component("do-list",{
            props:['title'],
            template:`
                <li>{{title}}<input type="button" value="移除" @click="$emit('remove')"></li>
            `
        })

        let app = new Vue({
            el:'#app',
            data:{
                newList:'',
                lists:[
                    {
                        id:1,
                        todoList:'aaa'
                    },
                    {
                        id:2,
                        todoList:'bbb'
                    },
                    {
                        id:3,
                        todoList:'ccc'
                    },
                    {
                        id:4,
                        todoList:'ddd'
                    }
                ]
            },
            methods:{
                addNewList:function(){
                    if(this.lists.length % 2 != 0){
                        this.lists.push({
                            id:this.lists.length + 1,
                            todoList:this.newList,
                        });
                        console.log("偶数");
                    }else{
                        this.lists.push({
                            id:this.lists.length + 1,
                            todoList:this.newList,
                        });
                        console.log("奇数");
                    }   
                }
            }
        })
        
    </script>
</body>
```