## dotnet一些命令
```
安装C#、C# Extensions、C# XML Documentation Comments扩展
dotnet -h | --help :获取有关可用命令和环境的信息
dotnet sln -h :打印出有关如何使用命令的说明
dotnet new sln :创建solution
dotnet new webapi :创建"ASP.NET Core Web API"
mkdir 文件夹名.Api
cd 进入文件夹
dotnet new webapi -h :获取有关可用命令
dotnet new webapi --no-https :关闭HTTPS.app.UseHsts和app.UseHttpsRedirection未添加到Startup.Configure中.
dotnet run :无需任何显式编译或启动命令即可运行源代码.
运行显示找不到localhost的网页，端口为5000.
在控制器中找到WeatherForecast
网址后加http://localhost:5000/WeatherForecast
cd .. ：返回上级目录
删除其他文件或文件夹，保留XXX.Api后缀的文件夹以及XXX.sln的文件
dotnet sln add APITest.Api(栗子):将项目添加到解决方案中
dotnet sln list：列出项目
mkdir APITest.Data：创建.Data文件夹
dir :查看文件
rm .\APITest.Data\ :删除.Data文件夹
dir :查看删除后的剩余文件或文件夹
```
## 创建一个解决方案的流程
```
创建一个解决方案
dotnet new sln

创建一个ApiTest.Api文件夹
mkdir ApiTest.Api

创建Api
dotnet new webapi --no-https

创建类库ApiTest.Data和ApiTest.Domain
mkdir ApiTest.Data
dotnet new classlib

另一种方法创建
dotnet new classlib -n ApiTest.Domain

添加解决方案
dotnet  sln add  ApiTest.Data
dotnet  sln add  ApiTest.Domain   

添加依赖包
dotnet add package Newtonsoft.json

添加项目
dotnet add ApiTest.Api reference ApiTest.Data
dotnet add ApiTest.Api reference ApiTest.Domain

用vs打开显示APITest.Api、APITest.Data、APITest.Domain并列
```
## dotnet批处理
[使用dotnet命令生成解决方案](https://gitee.com/myhfw003/dotnet-create-solution/blob/master/PictureCore.bat)
    克隆下来，以管理员身份运行
## 解决批处理乱码和卡顿问题
### 解决乱码问题
    打开文件进入编辑
    啥都不用改，另存为，在保存的时候将编码改为ANSI,保存
    于是乎就成了，按任意键冲就完了
### 卡顿问题
    1.手动解决
    windows cmd->属性->选项->编辑选项
    取消 快速编辑模式：
    cmd默认开启了“快速编辑模式”，只要当鼠标点击cmd任何区域时，就自动进入了编辑模式，之后的程序向控制台输入内容甚至后台的程序都会被阻塞.

    在控制台里面回车或者右键鼠标后，自动退出了编辑模式.因此，控制又恢复输出内容，服务端又正常了.

    选择快速编辑模式的时候,鼠标不小心点到cmd某个位置,都可能让正在运行的进程都卡住，直到按下回车后，会跳出一堆.

    2.自动解决

    拖动或点击CMD窗口造成程序阻塞，这是因为windows默认cmd窗口启用快速编辑模式，关闭即可.
    在bat文件中关闭cmd窗口的快速编辑模式，bat文件如下：
    (懒得插图了，把下面两行代码放置在@echo后面，就是倒数第二行)
    @echo off
    reg add HKEY_CURRENT_USER\Console /v QuickEdit /t REG_DWORD /d 00000000 /f
## 这样也能添加项目
![图](imgs/026.PNG)
## 下载这些插件
![图](imgs/027.PNG)
## 运行效果
![图](imgs/028.PNG)

