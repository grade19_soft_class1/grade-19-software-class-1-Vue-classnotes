## Vue-cli
## 使用npm或powershell安装
## 最好用npm下载（yarn好像不太顶）
```
npm install -g @vue/cli
OR
yarn global add @vue/cli
```
## 查看版本号
```
vue --version
```
## 在D盘创建一个myVueProject文件夹并进入
```
mkdir myVueProject

cd myVueProject
```
## 创建一个名为hello-world项目
```
vue create hello-world
```
## 选择项
```
第一个选：Default ([Vue 2] babel, eslint)

第二个选：Yarn
```
## 在D盘找到myVueProject文件夹用vscode打开
## 运行项目
```
yarn serve
```
## 下载vetur插件(保存后yarn serve就能自动运行)

## ❗❗❗❗解决yarn安装@vue/cli的问题
### 1.先安装yarn
```
npm i -g yarn
```
## 2.配置环境变量
```
此电脑属性→环境变量→系统变量（Path）→编辑→浏览→C盘→用户→administrator→AppData→Local→Yarn→bin→确定(over)
```

