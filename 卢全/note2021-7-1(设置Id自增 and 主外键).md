## 设置Id自增
## 在需要自增的Id前面添加
```
[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
```
## 并在顶部using
```
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

```
![](imgs/054.PNG)
## 最后
```
dotnet ef migrations add 初始化自增长     //初始化

dotnet ef database update   //迁移数据
```
![](imgs/055.PNG)
## 设置主外键
## 设置外键
## UserRoles.cs
![](imgs/056.PNG)
## 设置主键
## Users.cs和Roles.cs
![](imgs/057.PNG)
![](imgs/058.PNG)
# 記得using System.Collections.Generic;
