## 安装SQL server依赖包
```
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
```
## 添加配置
## 在appsettings.json文件添加连接信息
```
"ConnectionStrings": {
    "SqlServerConnection":"Server=.;Database=ApiTest;uid=sa;pwd=123456;"
  },
```
![](imgs/050.PNG)
## 在Startup.cs文件添加
```
var sqlConnection=Configuration.GetConnectionString("SqlServerConnection");

services.AddDbContext<ApiTest.Api.Db.ApiTestDb>(option=>option.UseSqlServer(sqlConnection));
```
![](imgs/051.PNG)
# 记得 using Microsoft.EntityFrameworkCore;
## 在数据库上下文ApiTestDb.cs添加
```
  public ApiTestDb(DbContextOptions<ApiTestDb> db):base(db)
        {

        }
```
![](imgs/052.PNG)
## 最后迁移一下数据
```
dotnet ef database update
```
![](imgs/053.PNG)
# OK

# 刪除数据库、migrations
```
dotnet ef database  drop

dotnet ef migrations remove
```
## 重新生成依赖包
```
dotnet restore
```

