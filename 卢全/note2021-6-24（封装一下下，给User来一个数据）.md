## 封装一下下，给User来一个数据,连接一下下sqllite
## 给User来一些数据
![图](imgs/034.PNG)
## 封装一下Get请求
![图](imgs/038.PNG)
![图](imgs/037.PNG)
## 分页
## Skip、Take
```
例:
var pageIndex=1,
var Take=20

Skip((pageIndex-1)*pageSize).Take(pageSize)

(pageIndex-1)*pageSize=0
pageSize=20

则跳过0条,显示：1-20

var pageIndex=2,
var Take=5

Skip((pageIndex-1)*pageSize).Take(pageSize)

(pageIndex-1)*pageSize=5
pageSize=5

则跳过0条,显示：6-10

```