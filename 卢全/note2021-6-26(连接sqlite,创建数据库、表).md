## 首先安装 Entity Framework Core
```
dotnet add package Microsoft.EntityFrameworkCore.Sqlite  //安装sqlite程序安装包
```
## 创建模型
## 定义构成模型上下文
![](imgs/039.PNG)
## 定义构成模型实体类
![](imgs/040.PNG)
![](imgs/041.PNG)
![](imgs/042.PNG)
## 创建数据库
### 以下步骤使用迁移创建数据库
```
dotnet tool install --global dotnet-ef   //安装dotnet ef

dotnet add package Microsoft.EntityFrameworkCore.Design  //安装dotnet ef 设计包

dotnet ef migrations add 初始化数据   // migrations 命令为迁移搭建基架，以便为模型创建一组初始表。

dotnet ef database update  //database update 命令创建数据库并向其应用新的迁移。
```
## 在模型上下文添加一行语句
```
  protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=D:\blogging.db");
```
![](imgs/043.PNG)
## 在D盘会有一个以Db结尾的数据库文件
![](imgs/044.PNG)
![](imgs/045.PNG)

