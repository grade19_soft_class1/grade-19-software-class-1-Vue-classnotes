# 学Vue的第五天
## 摸摸鱼
```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    #app {
        height: 200px;
        width: 200px;
    }
</style>

<body>
    <div id='app' :style="style1"  style="background-color:''">
        

    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let app = new Vue({
            el: '#app',
            data: {

            },
            computed: {

                style1: function () {
                    let arr = ['red', 'green', 'black','pink','yellow','brown','coral'];
                    let rnd = Math.floor(Math.random() * arr.length);
                    
                    return {
                        'background-color':arr[rnd],
                        
                    }

                }
                
            }

        })

    </script>
</body>

</html>
```
## 随机颜色div
![图](imgs/012.PNG)
![图](imgs/013.PNG)