# bookstack图书馆是个好东西！
## 首先打开Linux😊
## 1.创建Vue文件
## 检查npm版本
```
npm -v
```
## 检查yarn版本
```
yarn -v
```
## 使用国内的npm淘宝源来加速安装
```
yarn config set registry https://registry.npm.taobao.org
```
## yarn安装@vue/cli
```
yarn global add @vue/cli
```
## 创建一个名为hello-world的Vue文件
```
vue create hello-world
```
## 选择Vue2和Yarn
## 安装成功运行
```
yarn serve
```
## 打包(打包成功后会出现一个dist文件夹)
```
yarn build
```
## 把dist文件夹复制到 /var/www/ 并改名为 my-hello-world
```
cp -r /var/www/my-hello-world
```
## 2.部署到阿里云服务器
## 进入到 /var/www目录
```
cd /var/www
```
## 创建一个域名命名的文件夹如
```
mkdir mySoft1.9ihub.com
```
## 授予权限
```
chmod -R 777mySoft1.9ihub.com/
```
## 然后把打包好的dist文件放进 /var/www/mySoft1.9ihub.com文件夹(xftp)
## 进入目录 /etc/nginx/conf.d/
```
cd /etc/nginx/conf.d/
```
## 创建一个conf文件并编辑
```
vim mySoft1.9ihub.com



内容：

server{
    listen 80;监听的端口

    server_name mySoft1.9ihub.com;监听的域名

    location /{
        root; /var/www/mySoft1.9ihub.com;网站所在路径

        index index.html;默认的首页文件
    }
}

```
## 需要到阿里云解析相对域名
## 检查配置文件
```
nginx -t 
```
## 重载配置文件
```
nginx -s reload
```




