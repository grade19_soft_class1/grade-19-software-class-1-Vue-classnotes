 ## 刷新Token于Params创建RefreshToken.cs添加属性
```
public class RefreshTokenDTO
    {
        public string Token { get; set; } 
        public string refreshToken { get; set; }
    }
}
```
```
    于Utils中的TokenHelper.cs：
    public static string ValidateToken(TokenParameter tokenParameter,RefreshTokenDTO refresh)
        {
            var handler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal claim = handler.ValidateToken(refresh.Token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                }, out SecurityToken securityToken);

                return claim.Identity.Name;
            }
            catch
            {
                return null;
            }
        }
```
    于UsersController.cs添加：
    ```
    [AllowAnonymous]
    [HttpPost, Route("refreshtoken")]
    public dynamic RefreshToken(RefreshTokenDTO refresh)
        {
            
            var username=TokenHelper.ValidateToken(_tokenParameter,refresh);

            if (string.IsNullOrEmpty(username))
            {
                return new
                {
                    Code = 1002,
                    Data = "",
                    Msg = "token验证失败"
                };
            }

            var token = TokenHelper.GenerateToekn(_tokenParameter, username);
            var refreshToken = "112358";


            return new
            {
                Code = 1000,
                Data = new { Token = token, refreshToken = refreshToken },
                Msg = "刷新token成功^_^"
            };
        }
        ```