## 新的一门课程 Vue.js
## 一、第一步 做个简单的“Hello Vue”

```
<div id="app">
  {{ message }}
</div>
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
})
```
成功创建了第一个 Vue 应用！

## 二、安装Node.js步骤
1、下载对应你系统的Node.js版本:https://nodejs.org/en/download/
2、选安装目录进行安装
3、环境配置
4、测试
## 三、Vue三大组件库
iView、element ui、ant profession vue
