# 生命周期函数的理解

1. beforeCreate 

这是第一个生命周期函数 其实是在实例未被创建之前执行,此时Vue实例中的data methods还没有实例化

2. created 

Vue实例中的data methods属性数据已经初始化完成 但是dom未生成 $el未存在

3. beforeMountd 

其执行的模板已经在内存中编辑完成,但是未渲染到界面中
挂载前 vue实例的$el和data都已初始化，挂载之前为虚拟的dom节点，data.message未替换

4. mounted

其执行的模板已经挂载到了界面上

5. beforeUpdate

数据更新时调用 发生在虚拟DOM重新渲染和打补丁之前  当我们更改任何Vue数据 都会触发该函数

6. updated

数据更新就会触发

7. beforeDestory

在销毁对象前调用

8. destoryed

在销毁对象后调用


栗子看看

```


<body>

<div id="app">
    <p>Finding happy: {{msg}}</p>
    <li v-for="(item,key) in lists " :key="key">
        {{item}}
    </li>
</div>

   

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>


        var app = new Vue({
            el: "#app",
            data:{
                msg : "快乐星球",
                lists:['aa','bb','dd']
            },
            //时刻监测数据msg lists的变化 一但变化就会调用该函数
            watch:{
                msg:function(){
                    console.log('watch:','message变了')
                }
            },
            methods:{
                show:function(){
                    console.log("这是一个方法")
                }
            },
            //第一个生命周期函数 在实例未被创建前执行 此时的data与methods还未被初始化
            beforeCreate(){
                console.log(this.msg)
            }
            ,
            //第二个生命周期函数 此时的data methods已经初始化完成
            created(){
                console.log("第二个生命周期函数"+this.msg),
                //应为是通过v-for循环遍历li，所有created之前挂载阶段还没有开始
                console.log(document.getElementsByTagName("li").length), //1
                this.show()
            },
            //beforeMouted 其执行时模板已经在内存中编辑完成 但是未渲染在界面中
            beforeMount(){
                console.log(document.getElementById("app").innerText),
                //通过的v-for生成的html还没有被挂载到界面上
                console.log(document.getElementsByTagName("li").length)//1

            },
            //mounted 其执行时模板已经挂载到界面上 用户可以看见已经渲染好的页面
            //el被新创建的vm.$el替换 并挂载到实例上去调用钩子
            mounted(){
                console.log("mounted将模板挂载到界面上了",this.msg),               
                console.log(document.getElementsByTagName("li").length)//3
            },
            //数据更新时调用 发生在虚拟DOM重新渲染和打补丁之前  当我们更改任何Vue数据 都会触发该函数
            beforeUpdate(){
                console.log("beforeUpdate 钩子执行"),
                console.log("beforeUpdate 钩子执行"+this.msg)

            },
            //数据更新就会触发
            updated(){
                console.log("updated 钩子执行"),
                console.log("updated 钩子执行"+this.msg)

            },
            //实例销毁之前调用
            beforeDestroy: function (){
                console.log("beforeDestory 钩子执行")
            },
            //实例销毁后调用
            destroyed: function(){
                console.log("destoryed 钩子执行")
            }


        })


        app.$destroyed()
    </script>

</body>


```


![图片炸裂](./png/26.png)


把app.$destroy给注释之后 修改data里面的内容 会发现触发了watch 与 beforeUpdate 与updated函数


![图片炸裂](./png/27.png)


## 面试题 

1. 什么是生命周期函数

答: Vue实例是从创建到销毁的过程就是生命周期函数 

从创建到->初始化数据->编译模板-挂载Dom(渲染)->更新(渲染)->卸载的一系列过程

2. vue生命周期函数的作用

答: 它的生命周期有多个钩子,让我们在控制整个Vue实例更容易形成好的逻辑

3. vue生命周期包含了几个阶段

答: 包含了八个阶段 创建前/创建后/挂载前/挂载后/更新前/更新后/销毁前/销毁后

beforeCreate created beforeMount mounted beforeUpdate updated beforeDestroy destroyed


4. 第一次加载界面会触发什么钩子

beforeCreate created beforeMount mounted





补充$mount
当你vue没有挂在el时,可以使用#mount


### 钩子的实战用法

1. 异步函数


![图片炸裂](./png/28.png)

![图片炸裂](./png/29.png)


执行的顺序是created mounted 异步函数 updated



2. Vue.nextTick对异步函数的结果进行操作

在数据变化之后立即使用Vue.nextTick(callback) 这样回调函数在DOM更新完成后就会调用




