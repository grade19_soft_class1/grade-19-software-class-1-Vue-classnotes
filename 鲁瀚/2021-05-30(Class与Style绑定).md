# Class与Style绑定

```



<style>
    .active {
        background-color: brown;
    }

    .text {
        font-size: 46pt;

    }
</style>



<body>

    <div id="app">
        <div v-bind:class="classObject">123</div>

    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>



        var app = new Vue({
            el: '#app',
            data: {
                isActive: true,
                istext:true
              
            },
            computed: {
                classObject: function () {
                    return {
                        active: this.isActive ,
                        text:this.istext  
                    }
                }
            }

        })





    </script>





</body>


```


可以绑定一个返回对象的计算属性  这是一个常用且强大的模式


## 数组语法与对象语法


使用三元运算切换列表中的class


```


<body>

    <div id="app">
   
        <!-- <div v-bind:class="[isActive ? isActive : '',istext]">789</div> -->
     
        <div v-bind:class="[{ active: isActive },istext]">134</div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>



        var app = new Vue({
            el: '#app',
            data: {
                // isActive:"active",
                // istext:"text",
                isActive:true,
                istext:'text'
            },
        
        })





    </script>





</body>

```

![图片炸裂](./png/31.png)

既可以绑定数组与对象



## 绑定内联样式


```


   <div id="app">

        <div v-bind:style="{color:acolor ,fontSize:afontSize}">这是一首简单的小情歌</div>
        <div v-bind:style="[color]">Lucas</div>
        <div v-bind:style="styleObject">milu</div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>



        var app = new Vue({
            el: '#app',
            data: 
            {
             acolor:"green",
             afontSize:'30pt',
             color:{
                 color:'red',
                 fontSize:"20pt"
             },
             styleObject:{
                color:'pink',
                 fontSize:"20pt"
             }
            }
        
        })





    </script>


```


![tupian](./png/32.png)



```


 computed: {
                styleObject: function () {
                    return {
                        color: 'pink',
                        fontSize: "20pt"
                    }

                }
            }


```


对象语法常常与计算属性使用






