# 又是我！我又来了！
## 今天的话！我们就来慢慢摸索Vue中的好东西
## 首先胡哥是讲了如何渲染！其次是绑定事件，其实还是蛮简单的！就讲讲道理就完事了！
```
   <div id="app">
    <center><h1>{{msg}}</h1></center>    
    </div>

    <script src="./vue.js"></script>
    <script>
        let app = new Vue({
            el:"#app",
            data:{
                msg:"螺旋三爷"
            }
        })
    </script> 
```
## 这个还是比较可以的！接下来是另一种好玩的方式，绑定事件
```
    鼠标浮动流！
    就是你将鼠标放到字体的位置会有特别的信息出来！嘿嘿！
    <div id="app">
        <center>
            <h1>
                <span v-bind:title="msg">
                    螺旋？奥义？
                </span>
            </h1>
        </center>
    </div>

    <script src="./vue.js"></script>
    <script>
        let app = new Vue({
            el: "#app",
            data: {
                msg: "螺旋三爷"
            }
        })
    </script>
```
## 接下来就是传说中的小型If事件啦！这种就可以选择性输出！你觉得呢！
## 我感觉还蛮好玩！其实在网页就可以改他是true还是false
```
    <div id="app">
        <center>
            <h1>
                <span v-if="seen" v-bind:title="msg">
                    螺旋？奥义？
                </span>
                <span v-else>
                    青铜门终极？
                </span>
            </h1>
        </center>
    </div>

    <script src="./vue.js"></script>
    <script>
        let asd = new Vue({
            el: "#app",
            data: {
                msg: "螺旋BOSS三爷",
                seen:true
            }
        })
    </script>
    其实他那个网页改的Id是通过他New出来的实例所命名的!
    你以为是app.seen=false;
    其实是ads.seen=false;
    所以说并不是通过id来修改
```
## 接下来就是最简单的循环语句啦！这个循环语句讲道理应该也可以发掘一些好玩的东西吧！
```
    <div id="app">
        <table>
            <tr v-for="menu in menuList">
                <td>{{menu.id}}</td>
                <td>{{menu.name}}</td>
                <td>{{menu.duomiquli}}</td>
            </tr>
        </table>
        <button v-on:cliok="sayagin">再加一条？</button>
    </div>

    <script src="./vue.js"></script>
    <script>
        let asd = new Vue({
            el: "#app",
            data: {
                msg: "螺旋BOSS三爷",
                seen: true,
                menuList: [
                    {
                        id: 1,
                        name: '迪凯',
                        duomiquli: '308'
                    },
                    {
                        id: 2,
                        name: '跃某人',
                        duomiquli: '308'
                    },
                    {
                        id: 3,
                        name: '三爷',
                        duomiquli: '308'
                    },
                    {
                        id: 4,
                        name: '沦神',
                        duomiquli: '308'
                    }，
                ]
            }，
            //在这就可以加！
            methods:{
                sayagin:function(){
                    this.menuList.puch({
                        id:6,
                        name:'凭空飞起',
                        duomiquli:'308'
                    })
                }
            }
        })
    </script>
    这个相对来说也是蛮简单的！就是最基础的for循环输出
    没有什么太大的特别差距！
    还有特别一点的就是他可以加减
    
```
## 接下来这个就是表单获取文本值！这个的话还是比较有趣的！他可以跟着你所输入的值实时运转！
```
    <div id="app">
        <form>
            <label for="">用户名：</label><input type="text" v-model="formData.username">
            <label for="">密码：</label><input type="password" v-model="formData.password">
            <br>
            {{str}}
        </form>
    </div>

    <script src="./vue.js"></script>
    <script>
        let asd = new Vue({
            el: "#app",
            data: {
                msg: "螺旋BOSS三爷",
                seen: true,
                formData:{
                    username:'admin',
                    password:'123456'
                }
            },
            computed:{
                str:function(){
                    return`用户名为：${this.formData.username}，密码：${this.formData.password}`
                }
            }
        })
    </script>   
    看你操作啦！
```
## 还有最后一个啦！最后一个是获取表单组合！
```
    <div id="app">
        <ul>
            <todo-item v-for="item in list" v-bind:v="item" v-bind:key="item.id"></todo-item>
        </ul>
    </div>

    <script src="./vue.js"></script>
    <script>
        Vue.component('todo-item', {
            props: ['v'],
            template: '<li>{{v.name}}</li>'
        })
        let asd = new Vue({
            el: "#app",
            data: {
                msg: "螺旋BOSS三爷",
                seen: true,
                formData: {
                    username: 'admin',
                    password: '123456'
                },
                list: [
                    {
                        id:'1',
                        name:'今晚308直接起飞'
                    },
                    {
                        id:'2',
                        name:'预报说下雨'
                    },
                    {
                        id:'3',
                        name:'好像要下大雨了'
                    },
                    {
                        id:'4',
                        name:'算了！改天再起飞'
                    }
                ]
            },
        })
    </script>
    这里也有一个难题难到了胡哥，所以说！我们就可以尝试解答一波！试试能否在胡哥之前找到答案呀！~
```
## 今天也是快乐的一天哦！嘿嘿！
# 生活愉快