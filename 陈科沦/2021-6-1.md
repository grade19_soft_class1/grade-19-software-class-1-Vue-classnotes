# 哈喽哈！我又来啦！今天儿童节！这次呢！我们请来了三爷为我们解说这次笔记
## 解说是解说！但笔记还是我自己做的哈！问题不大！
## 我们也是非常荣幸的请到了308男团全体成员！
## 首先非常荣幸的能请来三爷来为我们解说儿童节当天的笔记！接下来的笔记就交给三爷咯！
## 今天的首位选手他出来了！他就是列表渲染
## 首先映入眼帘的还是V-for 1.0版本
```
    他为什么说是1.0版本呢？
    因为他所用到的东西都是最原始的引用！然后再将他们引用
    不多说了！上代码吧！你们一看就懂了！
    <div id="app">

        <ul>
            <li v-for="(item,a) in lists">
             {{a+1}} - {{item.name}}
            </li>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let sm = new Vue({
            el:'#app',
            data:{
                lists:[
                    {
                        name:'DiKai'
                    },
                    {
                        name:'SanYe'
                    },
                    {
                        name:'YueMouRen'
                    },
                    {
                        name:'LunShen'
                    },
                ]
            }
        })
    </script>
    是不是突然看懂了！
```
## 接下来的话我们就来讲解一下v-for1.5版本呀！
```
    这个1.5版本的话是用来输出整个对象的！也是还蛮不错的！
    <div id="app">

        <ul>
            <li v-for="(item,a) in lists">
             {{a}} - {{item}}
            </li>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let sm = new Vue({
            el:'#app',
            data:{
                lists:{
                    SheZhang:'迪凯',
                    SheYuan:'三爷',
                    SheYuanErHao:'跃某人',
                    SheYuanSanHao:'沦神',
                }
            }
        })
    </script>
```
## 接下来是个数组经常用到的方法!
```
    push():数组尾部添加
    pop():数组头部添加
    shift():数组尾部删除
    unshift():数组头部删除
    splice():万能公式
    sort():默认数组排序
    reverse():默认数组倒序
```
## 接下来就是最终版本啦！自由删减版本！帅气如我呀！！！
```
    <div id="app">

        <form>
            <input type="text" v-model="newItem">
            <input type="button" value="加一个" @click="add">
        </form>
        <ul>
            <do-item v-for="(item,index) in lists" :key="item.id" :title="item.name" @remove="lists.splice(index,1)">
            </do-item>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        Vue.component('do-item', {
            props: ['title'],
            template: `<li>{{title}}<input type="button" value="删除" @click="$emit('remove')"></li>`
        })
        let sm = new Vue({
            el: '#app',
            data: {
                newItem: '',
                lists: [
                    {
                        id: 1,
                        name: '迪凯',
                    },
                    {
                        id: 2,
                        name: '跃某人',
                    },
                    {
                        id: 3,
                        name: '三爷',
                    },
                ]
            },
            methods: {
                add: function () {
                    this.lists.push({
                        id: this.lists.length + 1,
                        name: this.newItem
                    })
                    this.newItem = ''
                }
            }
        })
    </script>
    这个版本是可以支持你自由进行增删的！加油哦！少年！
```