# 今天呢！胡哥也讲了一波！另外的小玩法！
## 一开始跟我们讲了！什么是Vue实例！在我的理解的话是跟New 一个对象是差不多的！组件实例和根实例也是差不多的！只是胡哥说不建议做多个根实例！一面别人看不懂！一面是会打乱实例树的节奏！
## 而且一个根实例也会使我们思路更加的清晰！所以
## 都给我只做一个实例！
## 接下来开始我们的第一个小内容呀！直接起飞！
## 首先！是做一个最简单的变量！然后在实例适用！
## 而且我们还做了个响应式！即parm.x改了！data.x也跟着改！反过来也是一样的！
```
    <div id="app">

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let parm = {
            x: 5432
        }
        let app = new Vue({
            el: '#app',
            data: parm

        })
        console.log(app.x === parm.x);
        console.log(app);
        
    </script>

    这个也是最简单的！
    输出的是true
    第二个输出的是整个app实例
```
## 这个还有一个注意的要点！就是他之前加的是可以加进去!
## 但是如果放到后面加他是没有是进入实例中的！
```
    <div id="app">

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let parm = {
            x: 5432
        }
        let app = new Vue({
            el: '#app',
            data: parm

        })

        parm.y='三爷YYDS'
        console.log(parm);
        console.log(app);
    </script>
    在之后他的输出是没有Y属性的！因为胡哥说就像！考试一样！这个就是十五分钟之前和开考十五分钟之后一样！
    在十五分钟之后就不让你进去啦！
    要修改也不是没有办法！
    要一开始就将data中的数据修改好！
    <div id="app">

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let parm = {
            newTodoText:'',
            visitCount:0,
            hideCompletedTodos:true,
            todos:[],
            error:null,
            x: 5432
        }
        let app = new Vue({
            el: '#app',
            data: parm

        })
        parm.y='三爷YYDS'
        console.log(parm.x===app.x);
        console.log(parm.y===app.y);
        console.log(app);
    </script>
    就像酱紫呀！
```
## 接下来是第二个呀！钩子函数呀！生命周期钩子。
```
    <div id="app">

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let parm = {
            newTodoText:'',
            visitCount:0,
            hideCompletedTodos:true,
            todos:[],
            error:null,
            x: 5432
        }
        let app = new Vue({
            el: '#app',
            data: parm,
            created:function(){
                console.log('能不能讲一波道理呀！');
            }

        })
    </script>
    这是最简单的钩子结构！
    然后就是稍微有一点点难度的钩子结构
    <div id="app">
        {{x}}
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let parm = {
            newTodoText:'',
            visitCount:0,
            hideCompletedTodos:true,
            todos:[],
            error:null,
            x: 5432
        }
        let app = new Vue({
            el: '#app',
            data: parm,
            created:function(){
                console.log('能不能讲一波道理呀！');
            },
            beforeUpdate:function(parm){
                console.log("这个是修改之前");
                console.log(this.$data.x);
            },
            updated:function(){
                console.log("这个是修改之后");
            }

        })
    </script>
    这个钩子函数的小结构也是比较有趣的！所以快去玩吧！！
```
## 今天就到这里啦！
## 今天也要是愉快的一天哦！！！！加油！
