## 在服务器中复制

```
cp -r dist/ /var/www/my-app
//dist 是要复制的文件夹 my-app 是重命名
```
```
推荐一个网站 : 书栈网
```
## 部署一个静态网站

```
//打包项目 dist
yarn build

cd /etc/nginx/conf.d

//创建conf文件
vim myhello.ygking.top.conf 

//配置文件
server{
        listen 80;  #监听的端口
                server_name myhello.ygking.top; #监听的域名 
                location / {
                        root  /var/www/myhello.ygking.top ;#网站所在路径
                        index index.html; #默认的首页文件
        }
}

将打包的dist里面的文件放到 /var/www/myhello.ygking.top 文件夹

去阿里云解析域名
```