## 对象语法

```
通过v-bind:class 指令传入的attribute可以与普通的attribute共存
v-bind:style 的对象语法十分直观——看着非常像 CSS，但其实是一个 JavaScript 对象。CSS property 名可以用驼峰式 (camelCase) 或短横线分隔 (kebab-case，记得用引号括起来) 来命名
```

```html
    <div v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
    <div
    class="static"
    v-bind:class="{ active: isActive, 'text-danger': hasError }">
    </div>
```
```js
    data: {
    activeColor: 'red',
    fontSize: 30
    }
    data: {
    isActive: true,
    hasError: false
}
```

## 数组语法

```
我们可以把一个数组传给 v-bind:class，以应用一个 class 列表
v-bind:style 的数组语法可以将多个样式对象应用到同一个元素上
```

```html
<div v-bind:class="[activeClass, errorClass]"></div>
<div v-bind:style="[baseStyles, overridingStyles]"></div>
```
```js
data: {
  activeClass: 'active',
  errorClass: 'text-danger'
}
```