## vue cli命令安装
可以使用下列任一命令安装这个新的包：
```
    npm install -g @vue/cli
    # OR
    yarn global add @vue/cli
```

```
    使用 npm 安装可以更改成淘宝镜像来下载会更快  
    npm config set registry https://registry.npm.taobao.org
    使用 yarn 也是一样的
    yarn config set registry https://registry.npm.taobao.org

```
安装完之后发现 'vue' 不是内部或外部命令，也不是可运行的程序

然后去配置类环境变量
```
用npm config list 查看配置信息 找到prefix的路径 
如果是用yarn 安装 就用 yarn global dir 查看yarn 的路径 
找到vue.cmd文件将这个地址加入环境变量
```
打开环境变量
```
    此电脑（右键打开属性）——高级系统设置（左侧）——环境变量——系统变量——path——将prefix后的地址填入
```

然后运行 npm install --global vue-cli 命令

后面发现 vue --version 可以用了
