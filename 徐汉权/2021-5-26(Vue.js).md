# Vue.js
## 过滤器
```
<body>
<div id="app">
{{date | formDate}}
</div>

    <script src="./vue.js"></script>

<script>
let paddate = function(value){
    return value < 10 ? '0' + value : value; 
};
let app = new Vue({
    el:'#app',
    data:{
        date: new Date()
    },
    filters:{
        formDate :function(value){
            let date = new Date(value);
            let year = date.getFullYear();
            let month = paddate(date.getMonth() + 1);
            let day = paddate(date.getDate());
            let hours = paddate(date.getHours());
            let minutes = paddate(date.getMinutes());
            let seconds = paddate(date.getSeconds());
            return year +'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
        }
    },
    beforeDestroy:function(){
        if(this.timer){
            clearInterval(this.timer);//在Vue实例销毁前，清除我们的定时器
        }
    }
})
</script>
</body>
```

## 2.2 指令与事件
### 指令(Directives)，它带有前缀v-,如:v-html,v-pre，v-if
### 以v-if为例
```

<div id="app">
<p v-if="show">
显示这段文本
</p>
</div>

    <script src="./vue.js"></script>

<script>

let app = new Vue({
    el:'#app',
    data:{
        show:true//fales
    }
})

</script>

```
## 【v-bind】
### 用途是:动态更新Html元素上的属性，比如Id,class等 
```
<div id="app">

<a v-bind:href=url>
    链接
</a>

<img v-bind:src="imgUrl">

</div>

    <script src="./vue.js"></script>

<script>
let app = new Vue({

    el:'#app',
    data:{
        url:'https://github.com',
        imgUrl:'http://xxx.xxx.xx/img.png'
    }
    
})
</script>
```
## 【v-on】
### 用途是绑定事件监听器，这样就可以做一些交互
```
<div id="app">

<p v-if="show">hahahahahahahahah</p>
<button v-on:click="handleClose">点击隐藏</button>

</div>

    <script src="./vue.js"></script>

<script>

let app = new Vue({
    el:'#app',
    data:{
        show:true
    },
    methods:{
        handleClose:function(){
            this.show = false;//true显示与不显示
        }
    }
})

</script>
```
### 在Button按钮上绑定一个v-on点击事件，v-on可以监听原生的DOM事件
```

<div id="app">

<p v-if="show">hahahahahahahahah</p>
<button v-on:click="show = false">点击隐藏</button>

</div>

    <script src="./vue.js"></script>

<script>

let app = new Vue({
    el:'#app',
    data:{
        show:true
    }
})

</script>

```
### 如果绑定的事件要处理复杂的业务逻辑，建议还是在methods里声明一个方法，这样可读性更强也好维护。
```

<div id="app">

<p v-if="show">这是一段文本</p>
<button v-on:click="handleClose">点击隐藏</button>

</div>

    <script src="./vue.js"></script>

<script>

var app = new Vue({
    el:'#app',
    data:{
        show:true
    },
    methods:{
        handleClose:function(){
            this.close();
        },
        close:function(){
            this.show = false;
        }
    }
})

</script>

```
## 也可以向一下方法这样使用
```

    <div id="app">



    </div>

    <script src="./vue.js"></script>

<script>
let app = new Vue({
    el:'#app',
    data:{
        show:true
    },
    methods:{
        init:function(text){
            console.log(text);
        }
    },

    mounted:function(){
        this.init('在初始化的时候调用');
    }
})
app.init('通过外部调用')

</script>
 
```

## 第二章节知识点:
```
Vue实例中:
el选项:选择Html中已经存在的DOM元素来挂载Vue实例
data选项:像是一个数据块，向已经被挂载的DOM元素传输块里的数据
 ------------------------------------
 钩子函数:
 created:数据初始化的时候调用的
 mounted:挂载el实例后调用的
 beforeDestroy:销毁数据之前的时候调用的
----------------------------------
关于指令
v-html
→不想输出纯文本，想要来点事件的可以使用这个
v-pre
→跳过它元素和子元素的编译过程
→但是Dom被Vue实例挂载后Dom元素调用后{{这里会显示Vue实例的Id}}
→而它本身的内容不会被编译出来
v-bind
→更新Html的属性，比如Id,Class,Url等
v-on
→用于绑定事件监听器，可以做一些交互
→就是说v-on绑定到一个按钮上，我监听这个按钮，有人动我一下
→我就和你产生一些交互
v-if
→暂时只了解到
→它能移除Vue里的数据
→True(移除)//false(显示)
```

## 2.3 语法糖
```
这是v-bind的缩写
<·······················>
<a v-bind:herf="url">链接</a>
<img v-bind:src="imgUrl">
<--可以缩写为:-->
<a :herf="Url">链接</a>
<img :src="imgUrl">

这是v-on的缩写
<----------------------->
<button v-on:click="handleClose">点击隐藏</button>
<--可以缩写为:-->
<button @click="handleClose">点击隐藏</button>
```