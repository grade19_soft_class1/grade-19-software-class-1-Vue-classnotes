# 条件渲染
## v-for
### 我们可以用 v-for 指令基于一个数组来渲染一个列表。v-for 指令需要使用 item in items 形式的特殊语法，其中 items 是源数据数组，而 item 则是被迭代的数组元素的别名。

![图片错误](./img/25.jpg)

### 在 v-for 块中，我们可以访问所有父作用域的 property。v-for 还支持一个可选的第二个参数，即当前项的索引。

![图片错误](./img/26.jpg)

你也可以用 of 替代 in 作为分隔符，因为它更接近 JavaScript 迭代器的语法：
```
<div v-for="item of items"></div>
```

## 在 v-for 里使用对象
## 你也可以用 v-for 来遍历一个对象的 property(属性)

```
<ul id="v-for-object" class="demo">
  <li v-for="value in object">
    {{ value }}
  </li>
</ul>
```
```
new Vue({
  el: '#v-for-object',
  data: {
    object: {
      title: 'How to do lists in Vue',
      author: 'Jane Doe',
      publishedAt: '2016-04-10'
    }
  }
})
```
## 当我们是个对象的时候可以返回三个值

![图片错误](./img/27.jpg)


### 在遍历对象时，会按 Object.keys() 的结果遍历，但是不能保证它的结果在不同的 JavaScript 引擎下都一致。


# 维护状态
当 Vue 正在更新使用 v-for 渲染的元素列表时，它默认使用“就地更新”的策略。如果数据项的顺序被改变，Vue 将不会移动 DOM 元素来匹配数据项的顺序，而是就地更新每个元素，并且确保它们在每个索引位置正确渲染。这个类似 Vue 1.x 的 track-by="$index"。

这个默认的模式是高效的，但是只适用于不依赖子组件状态或临时 DOM 状态 (例如：表单输入值) 的列表渲染输出。

为了给 Vue 一个提示，以便它能跟踪每个节点的身份，从而重用和重新排序现有元素，你需要为每项提供一个唯一 key attribute：
```
<div v-for="item in items" v-bind:key="item.id">
  <!-- 内容 -->
</div>
```
建议尽可能在使用 v-for 时提供 key attribute，除非遍历输出的 DOM 内容非常简单，或者是刻意依赖默认行为以获取性能上的提升。

因为它是 Vue 识别节点的一个通用机制，key 并不仅与 v-for 特别关联。后面我们将在指南中看到，它还具有其它用途。

不要使用对象或数组之类的非基本类型值作为 v-for 的 key。请用字符串或数值类型的值。

更多 key attribute 的细节用法请移步至 key 的 API 文档。


## 数组更新检测
变更方法
Vue 将被侦听的数组的变更方法进行了包裹，所以它们也将会触发视图更新。这些被包裹过的方法包括：

push()
pop()
shift()
unshift()
splice()
sort()
reverse()
你可以打开控制台，然后对前面例子的 items 数组尝试调用变更方法。比如 
```
example1.items.push({ message: 'Baz' })。
```

替换数组
变更方法，顾名思义，会变更调用了这些方法的原始数组。相比之下，也有非变更方法，例如 filter()、concat() 和 slice()。它们不会变更原始数组，而总是返回一个新数组。当使用非变更方法时，可以用新数组替换旧数组：
```
example1.items = example1.items.filter(function (item) {
  return item.message.match(/Foo/)
})
```
你可能认为这将导致 Vue 丢弃现有 DOM 并重新渲染整个列表。幸运的是，事实并非如此。Vue 为了使得 DOM 元素得到最大范围的重用而实现了一些智能的启发式方法，所以用一个含有相同元素的数组去替换原来的数组是非常高效的操作。

注意事项
由于 JavaScript 的限制，Vue 不能检测数组和对象的变化。深入响应式原理中有相关的讨论。


# 显示过滤/排序后的结果
有时，我们想要显示一个数组经过过滤或排序后的版本，而不实际变更或重置原始数据。在这种情况下，可以创建一个计算属性，来返回过滤或排序后的数组。

例如：

![图片错误](./img/28.jpg)



在计算属性不适用的情况下 (例如，在嵌套 v-for 循环中) 你可以使用一个方法：
```
<ul v-for="set in sets">
  <li v-for="n in even(set)">{{ n }}</li>
</ul>
data: {
  sets: [[ 1, 2, 3, 4, 5 ], [6, 7, 8, 9, 10]]
},
methods: {
  even: function (numbers) {
    return numbers.filter(function (number) {
      return number % 2 === 0
    })
  }
}
```


## 在 v-for 里使用值范围
v-for 也可以接受整数。在这种情况下，它会把模板重复对应次数。
```
<div>
  <span v-for="n in 10">{{ n }} </span>
</div>               // 1,2,3,4,5,6,7,8,9,10
```


# 列表渲染
## 如何做一个如下图所示的东西呢??

![图片错误](./img/42.jpg)

## 代码如下:
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>
<body>
    <div id="app">
        <form action="">
            <input type="text" v-model="newItem">
            <input type="button" value="添加" @click="addToLists">
        </form>

        <ul>
            <do-item v-for="(item,index) in lists" :key="item.id" :title="item.todoItem" @remove="lists.splice(index,1)"></do-item>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script>
        Vue.component("do-item",{
            props:['title'],
            template:`<div>
                <li>{{title}}</li>
                <input type="button" value="移除" @click="$emit('remove')">
                </div>`
        })

        var app=new Vue({
            el:"#app",
            data:{
                newItem:'',
                lists:[
                    {
                        id:1,
                        todoItem:"大雁向南飞"
                    },
                    {
                        id:2,
                        todoItem:"今天的天气这么好??"
                    },
                    {
                        id:3,
                        todoItem:"孔雀东南飞"
                    }
                ]
            },
            methods:{
                addToLists:function(){
                    console.log(this.newItem);
                    this.lists.push({
                        id:this.lists.length+1,
                        todoItem:this.newItem
                    })
                }
            }
        });
        
    </script>
</body>
</html>
```
## 上图Vue.component中的　 template:`  ` 如果没有 `<div></div>` 框住就会如下图


![图片错误](./img/42.jpg)


## 报错：Component template should contain exactly one root element. If you are using v-if on multiple elemen

## 组件模板应该只包含一个根元素。如果在多个元素上使用v-if，那么使用v-else-if将它们链接起来

## 组件模板中有li和input两个根元素，而Vue组件只允许用于一个根元素

## 修改一下，用一个div标签，将这他们包裹起来：