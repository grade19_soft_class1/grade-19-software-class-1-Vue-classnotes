# 路由

## 安装

### 首先找到脚手架的package.json 在其中的dependencies 中添加 
```
"vue-router":"latest"
```

### 在其中我们会发现 dependencies 和下面的 devdDependencies 有什么区别 其实 项目上传中只有dependncies 会被打包 而devdDependencies只是为了方便开发使用而已


## 注册路由 
首先我们先简单写一个login的组件 
```
<template>
  <table>
      <tr>
          <td><label for="">用户名：</label></td>
          <input type="text" placeholder="请输入用户名">
      </tr>
      <tr>
          
          <td><label for="">密码：</label></td>
          <input type="password" placeholder="请输入密码">
      </tr>
  </table>
</template>

<script>
export default {

}
</script>

<style>

</style>
```

我们可以先在App.vue 假设为主页

![图片错误](./img/57.jpg)

现在在js 里 首先必须通过Vue.use()以下方式显式安装路由器
```
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
```

![图片错误](./img/58.jpg)

## 记住在app.vue中还需要写入出口

## 在main.js 方法中的十四行我们注意到 为什么要写这一行 有什么用呢？？(这是与路由的两种形式有关)

如果我们将   mode:"history" 删除结果就是登录界面出不来 这是为什么呢？ 而且在输入地址的时候地址后还会出现非常丑陋的 #/ 

![图片错误](./img/59.jpg)


# 浅析使用vue-router实现前端路由的两种方式
## 关于vue-router
由于最近的项目中一直在使用vue，所以前端路由方案也是使用的官方路由vue-router，之前在angularJS项目中也是用过UI-router，感觉大同小异，不过很显然vue-router更友好一些。本文就以vue-router为例浅析我所理解的前端路由，具体用法和一些基本语法就不一一介绍了，官方文档说的更加清晰

### 关于前端路由

### 首先先了解什么是hash 和 history 
hash —— 即地址栏 URL 中的 # 符号（此 hash 不是密码学里的散列运算）。比如这个 URL：http://www.abc.com/#/hello，hash 的值为 #/hello。它的特点在于：hash 虽然出现在 URL 中，但不会被包括在 HTTP 请求中，对后端完全没有影响，因此改变 hash 不会重新加载页面。
history —— 利用了 HTML5 History Interface 中新增的 pushState() 和 replaceState() 方法。（需要特定浏览器支持）这两个方法应用于浏览器的历史记录栈，在当前已有的 back、forward、go 的基础之上，它们提供了对历史记录进行修改的功能。只是当它们执行修改时，虽然改变了当前的 URL，但浏览器不会立即向后端发送请求。




据我所知，在现在这些MVC和MVVM框架兴起之前，是不存在前端路由的，页面之间的跳转是由后台控制的。随着前后端分离和单页面应用（SPA）的兴起和WEB项目复杂度的增加，再加上前面这些框架的支持，慢慢前端路由也就成为了现实。单页面应用的特点就是可以在改变URL在不重新请求页面的情况下更新页面视图。

"更新视图但不重新请求页面"是前端路由的原理的核心之一，目前在浏览器环境中这一功能的实现主要有两种方式

### 利用URL中的hash（"#"）
利用History interface在 HTML5中新增的方法
下面我们就来看看vue-router是如何通过这两种方式来实现前端路由的

vue-router实现前端路由的方法和对比
使用过vue-router的都知道，在vue-router中有mode这样一个参数，这个参数的可选值有"hash"、 "history"、"abstract"
```
const router = new VueRouter({
  mode: 'history',
  routes: [...]
})
```
对应我们上面讲到的两种方式来说，hash就是第一种方式，history就是第二种方式，而第三种是在nodejs下的默认实现方式。

### 那"hash"和"history"这两种方式各有什么优劣呢？

首先在vue-router中默认使用的是hash这种方式，因为这种方式虽然带个#有点丑（官方竟然都这样说），但是不存在兼容性问题
而history由于底层的实现根据MDN的介绍，调用history.pushState()，所以存在浏览器兼容性问题。
如果不考虑兼容性问题的话，pushState肯定比只修改hash值更加强大，因为可以设置任意同源URL
pushState可以设置和当前URL一模一样，这样也会把记录添加到栈中，而hash设置的新值必须和原来不一样
还有，就算不考虑兼容问题的话，history模式还有一个问题，就是history模式会将URL修改的和正常请求后端的URL一样
```
http://oursite.com/user/id
```
这样的话如果后端没有配置对应的user/id这样一个地址的话就会返回404，官方推荐的解决办法是在服务端增加一个覆盖所有情况的候选资源：如果 URL 匹配不到任何静态资源，则应该返回同一个 index.html 页面，这个页面就是你 app 依赖的页面。同时这么做以后，服务器就不再返回 404 错误页面，因为对于所有路径都会返回 index.html 文件。为了避免这种情况，在 Vue 应用里面覆盖所有的路由情况，然后在给出一个 404 页面。（这种方案我还没实践过，有机会要实践一下）
所以综合考虑来说用在一些中后台项目中的话一般直接就采用hash这种默认方式了，而前台项目的话看需求选择使用history还是hash
 
