### Class与style绑定：(可绑定字符串，对象或数组)
```
来点Style(对象)
 
  <style>
      
        .redcolor{
            color: red;
        }
        .pinkcolor{
            color: pink;
        }


    </style>

 <div v-bind:class="{pinkcolor:pink,redcolor:red}">粉色 /红色</div>

  let app=new Vue({
            el:"#app",
            data:{
                pink:true,//false显示red
                red:true

            }


        })


//////优化方法1.0，让他绑定一个对象返回数据
<div v-bind:class="classObject"></div> 

 data:{
    classObject:{
    redcolor:false,
    pinkcolor:true
    }
    }
               
///////优化方法2.0，绑定一个返回对象的计算属性computed
<style>
      
        .redcolor{
            color: red;
        }
        .pinkcolor{
            color: pink;
        }


    </style>

  <font :style="styleObject">你看我是不是五颜六色</font>
    <script>
       let app=new Vue({
        el:"#app",
        data:{
            classObject:{redcolor:true,pinkcolor:true}


        },
        computed:{


            classObject01:function(){

                let  isChendada=false;
                if('Chendada'){
                    isChendada=true;
                }
                return  {
                    redcolor:isChendada,
                    pinkcolor:false

                }
            },
            styleObject: function () {
                    let arr = ['gold', 'pink', 'blue', 'green', 'yellow']
                    let rnd1 = Math.floor(Math.random() * arr.length)
                    let rnd2 = Math.floor(Math.random() * arr.length)
                    while (rnd1 === rnd2) {
                         rnd2 = Math.floor(Math.random() * arr.length)
                    }
                    return {
                        color: arr[rnd1],
                        'background-color': arr[rnd2]
                    }
                }

        }
    })




```
