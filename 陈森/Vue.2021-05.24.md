### vue介绍  
```
web的三大框架：React，Vue，Angular
Vue  是一套用于构建用户界面的渐进式框架


目前Android应用开发中较流行的编程模式有MVC、MVP、MVVM三种。
 传统的MVC模式分为：Model、View、Controller

 MVVM响应式编程模型,避免直接操作DOM , 降低DOM操作的复杂性

1.MVVM：页面输入改变数据，数据改变影响页面数据展示与渲染
2.M（model）：普通的javascript数据对象
3.V（view）：前端展示页面
4.VM（ViewModel）：用于双向绑定数据与页面，对于我们的课程来说，就是vue的实例






```
#### VUE核心功能
![](./imgs/xin.png)


#### 入门
```
开局Vue3.0
一个最简单的vue程序：
<body>
    <div id="vuejs">{{msg}}</div>
    <script src="https://unpkg.com/vue@next"></script>`
    <script>
      const  iyyy={
          data(){
              return {
                  msg:"hello,vue"
              }
          }
      }
      Vue.createApp(iyyy).mount('#vuejs')
    </script>
</body>



安装3.0：
npm  i -g yarn
切换到/目录初始化一个文件夹
yarn create @vitejs/app myviteApp
可能出现问题，需要更新node.js版本（version>12.0.0）

初始化一个myviteApp：
yarn
yarn dev 






```







