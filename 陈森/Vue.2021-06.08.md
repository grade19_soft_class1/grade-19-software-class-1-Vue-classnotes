### 安装vue 脚手架
```
安装vue脚手架:   npm i -g @vue/cli

查看是否版本：vue --version

创建项目：先选择创建在哪个目录
cd d:
vue create my-vue
然后选择vue的版本（vue2）/ ,选择yarn 

创建完后打开项目运行:yarn serve 就跑起来了
打包就可以部署到服务器了：yarn build



```
### 部署到linux
```
安装好@vue/cli后，项目打包好后（yarn build）
用“绿色蜗牛”把dist文件夹里的文件放到 /var/www/文件夹名
到阿里云解析一个二级域名：
在/etc/nginx/conf.d路径下配置二级域名的nginx：
我解析了一个myvue的名字然后如下：
server {


listen 80;
server_name myvue.sen98.top;

location / {
	root /var/www/MyVue;   /*访问文件夹目录*/
	index index.html;
}


}
nginx -t 查看配置状态
nginx -s reload 重启nginx

一个静态网页就部署好了

创建文件夹
命令 mkdir+文件夹名字

删除文件夹 :
删除空目录:find . -type d -empty -delete  

. 表示在当前目录执行

-type d 只搜索目录

-name 指定目录名称

rm -rf 强制执行rm命令删除所有目录即内容






```