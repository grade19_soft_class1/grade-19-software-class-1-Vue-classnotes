### 学习vue-router
```
在package包里添加依赖："vue-router":"latest"
还有一种开发版本，不会被打包起来的:yarn add vuex -D

app.vue文件：
添加一个路由出口返回： <router-view></router-view>


main.js文件:
//引入//
import VueRouter from  'vue-router'
Vue.use(VueRouter)


import HelloWorld from './components/HelloWorld.vue';
import Login from './Login';//引入登录
import Register from './Register';//引入注册


然后就是正常的使用了：
let router = new VueRouter({
  mode:'history',
  routes:[
    {
      path:'/',
      component:HelloWorld
    },
    {
      path:'/login',
      component:Login
    },
    {
      path:'/register',
      component:Register
    },
  ]

})

new Vue({
  router:router,
  render: h => h(App),
}).$mount('#app')


新建一个注册的Register.vue文件：
<template>
  <table>
    <tr>
      <td>
        <label for="">用户名：</label></td>
        <td><input type="text" placeholder="请登录" />
      </td>
    </tr>

    <tr>
      <td>
        <label for="">密 码：</label></td>
        <td><input type="password" placeholder="请输入密码" />
      </td>
    </tr>
<tr>
    <td>
      <label for="">确认密码：</label></td>
      <td><input type="password" placeholder="在输入一次密码" />
    </td>
</tr>
<tr>
    <td><router-link to="/login">取消</router-link></td>
</tr>


  </table>



</template>

其它页面同理
```