#### 事件处理
```
监听事件：
  <button v-on:click="counter += 1">与我击剑🤺🤺</button>
  <p>The button above has been clicked {{ counter }} times.</p>//获取用户点击次数

  data: {counter: 0}



  事件处理方法：
   <button v-on:click="counter ">与我击剑🤺🤺</button>

    let app = new Vue({
            el: '#app',
            data: {
                name: '咦，他很惊呼'
            },
            methods: {
                counter: function (event) {
                    console.log(this.name);
                    if (event) {
                        console.log(event);   // `event` 是原生 DOM 事件
                        console.log('原来如此');
                    }
                }

            }
        })


内联处理器中的方法:
 <button v-on:click="counter('我是内联的语句boom') ">与我击剑🤺🤺</button>
  <button v-on:click="counter('你怎么会在这里') ">与我击剑🤺🤺</button>

  let app = new Vue({
            el: '#app',
            data: {

            },
            methods: {
                counter: function (event) {
                    console.log(event);

                }

            }
        })



```
#### 事件修饰符
```
常用的有:
.stop
.prevent
.capture
.self
.once
.passive//不要.passive 和 .prevent 一起使用，因为 .prevent 将会被忽略


使用修饰符时，顺序很重要；相应的代码会以同样的顺序产生。因此，用 v-on:click.prevent.self 会阻止所有的点击，而 v-on:click.self.prevent 只会阻止对元素自身的点击。

```
![](./imgs/xiu.png)



#### 按键修饰符
```
常用的按键：
.enter => // enter键
.tab => // tab键
.delete (捕获“删除”和“退格”按键) => // 删除键
.esc => // 取消键
.space => // 空格键
.up => // 上
.down => // 下
.left => // 左
.right => // 右

全局自定义 config.keyCodes：
// 可以使用 `v-on:keyup.f1`
Vue.config.keyCodes.f1 = 112



使用：
<input type="text" @keyup.enter="submitInput"> //按下enter时触发事件

var app = new Vue({
          el: '#app',
          data: {},
          methods: {
            submitInput() {
              console.log('你按下了 enter 键')
            }
          }
        })



 .exact 修饰符：
.exact 修饰符允许你控制由精确的系统修饰符组合触发的事件。

<!-- 即使 Alt 或 Shift 被一同按下时也会触发 -->
<button v-on:click.ctrl="onClick">A</button>

<!-- 有且只有 Ctrl 被按下的时候才触发 -->
<button v-on:click.ctrl.exact="onCtrlClick">A</button>

<!-- 没有任何系统修饰符被按下的时候才触发 -->
<button v-on:click.exact="onClick">A</button>




```
### 来个文字游戏
```
<div id="app">
        <center><h1>
        <input type="text" placeholder="  按下f键进入坦克" @keyup.f="f"><br />

        <input type="text" placeholder=" 按下alt+ctrl触发" @keydown.alt.ctrl="alt"><br />

        <input type="text" placeholder="按下空格开始" @keyup.space="space"><br />


        <input type="text" placeholder=" 前进" @keyup.up="up"><br />

        <input type="text" placeholder=" 掉头" @keyup.down="down"><br />

        <input type="text" placeholder=" 向左" @keyup.left="left"><br />

        <input type="text" placeholder="向右" @keyup.right="right"><br />


        <input type="text" placeholder=" 弹起回车触发" @keyup.enter="enter"><br />
    </h1></center>
    


    </div>

    <script src="./vue.min.js"></script>
    <script>
        // 使用自定义按键需要对应键码
        Vue.config.keyCodes={
            f:70
        }
           
        
        var app = new Vue({
            el: '#app',
            methods: {
                f: function () {
                    console.log("你进入了坦克");

                },
                alt: function () {
                    console.log("你按下了alt+ctrl，准备");

                },
                space: function () {
                    console.log("确认开始");

                },
                up: function () {
                    console.log("你尝试前进打它一炮弹");

                },
                down: function () {
                    console.log("你选择了掉头逃跑");

                },
                left: function () {
                    console.log("你操作了左转");

                },
                right: function () {
                    console.log("你操作了右转");

                },
                enter: function () {
                    console.log("你的操作失误，坦克爆炸了");

                }
            }
        })


    </script>
```
放一个键码的网址：https://www.cnblogs.com/wuhua1/p/6686237.html