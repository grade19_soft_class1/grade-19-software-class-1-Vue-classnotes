#### 组件基础
```


全局注册：
Vue.component('my-component-name', {
  // ... 选项 ...
}) 




组件复用：
<div id="app">
  <button-counter></button-counter>
  <button-counter></button-counter>
  <button-counter></button-counter>
</div>

Vue.component('button-counter', {
            data: function () {
                return {
                    count: 0
                }
            },
            template: '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
        })
        let app=new Vue({
            el:'#app'
            
        })

data必须是一个函数；



通过 Prop 向子组件传递数据：
<div id="app">
        <blog-post title="我是开动物园的"></blog-post>
        <blog-post title="🐅🐇🐂🐎🐉🐍"></blog-post>
        <blog-post title="我看你像开马戏团的"></blog-post>
</div>
 Vue.component('blog-post', {
            props: ['title'],
            template: '<h3>{{ title }}</h3>'
        })
        let app = new Vue({
            el: '#app'

        })
动态传递： 
 <ul><blog-post v-for="post in posts" :key="post.id" :title="post.title"></blog-post> </ul>
Vue.component('blog-post', {
            props: ['title'],
            template: '<h3>{{ title }}</h3>'
        })
        let app = new Vue({
            el: '#app',
            data: {
                posts: [
                    { id: 1, title: '我是开动物园的' },
                    { id: 2, title: '🐅🐇🐂🐎🐉🐍' },
                    { id: 3, title: '我看你像开马戏团的' }
                ]
            }
        })



单个根元素：每个组件必须只有一个根元素，可以将模板的内容包裹在一个父元素内，来修复这个问题


通过插槽分发内容：
<alert-box>
  Something bad happened.
</alert-box>

Vue.component('alert-box', {
  template: `
    <div class="demo-alert-box">
      <strong>Error!</strong>
      <slot></slot>
    </div>
  `
})










```