#### 今天是六一，想当年.....
#### 

```
列表渲染：v-for （类型：item in items 关键词：in）//参数1~3
<ul>
    <li v-for="(item,index ) in  lists">//双参数
       {{index+1}} {{item}}
    </li>
</ul>
<script>
    let app = new Vue({
            el: "#app",
            data: {
                lists: 
                    lists: [
                    {
                        name: 'java',
                        
                    },
                    {
                        name: 'html'
                    },
                    {
                        name: 'js'
                    },

                ]
             
            }
        })
 </script>

```
![](./imgs/66.png)
#### 在组件上使用 v-for
```
表单：

 <form >

        <input type="text" v-model="newIndx">
        <input type="button" value="添加" @click="addlists">
</form>
<ul>
   <do-item v-for="(item,index) in lists" :key= "item.id" :title="item.name" @remove="lists.splice(index,1)">
            </do-item>
</ul>
 <script>
  Vue.component('do-item', {
            props: ['title'],
            template: `<li>{{title}}<input type="button" value="移除" @click="$emit('remove')"></li>`
        })


 let app = new Vue({
            el: "#app",
            
            data: {
                newIndx:'',
                lists: [
                    {   id:1,
                        name: 'java',
                        
                    },
                    {   id:2,
                        name: 'html'
                    },
                    {   id:3,
                        name: 'js'
                    },

                ]
            },
            methods:{
                    addlists:function(){
                    this.lists.push({
                        id:this.lists.length+1,
                        name:this.newIndx
                        
                    })
                     this.newIndx = ''

                    }
            }
        })


  </script>      


```

