#### vue实例
```
根实例：一个 Vue 应用由一个通过 new Vue 创建的根 Vue 实例；
所有的 Vue 组件都是 Vue 实例，并且接受相同的选项对象 

let app=Vue({})//一个项目一般只设置一个根实例
响应式系统,当property 的值发生改变，视图将会产生“响应”，会重新渲染
let sen = { x: 1 }
var app = new Vue({
  el: '#app',
  data: sen
})
sen.t="新增一个数据";
console.log(app.x===sen.x);//true
console.log(app.t===sen.t);//false

只有当实例被创建时就已经存在于 data 中的 property 才是响应式的，所以我们要设置默认值
data: {
  newTodoText: '',
  visitCount: 0,
  hideCompletedTodos: false,
  todos: [],
  error: null
}

```
#### 实例生命周期钩子（函数）
```
   let pram = {
            x: 3397
        }
        let app = new Vue({
            el: "#app",
            data: pram,
            created: function () {
              
                console.log("现在为：")
            },
            beforeUpdate: function (data) {
                console.log('修改进行');
                console.log(this);
                console.log(this.x);

            },
            updated: function (data) {
                console.log('修改进行后');
                console.log(this.$data);
            }
        })

箭头函数不能用this
```