#### 嵌套路由
```
介绍一下Vue的三大组件库：ElementUI、iView、ant design vue 设计框架

例：在User的目录下嵌套路由
要在嵌套的出口中渲染组件，需要在 VueRouter 的参数中使用 children 配置：
let router = new VueRouter({
  mode:'history',
  routes: [
    {
      path: '/user/:id',
      component: User,
      children: [
        // 当 /user/:id 匹配成功，
        // Username 会被渲染在 User 的 <router-view> 中
        { path: 'Username', component: Username }

        // ...其他子路由
      ]
    }
  ]
})
然后就可以套娃了：


```