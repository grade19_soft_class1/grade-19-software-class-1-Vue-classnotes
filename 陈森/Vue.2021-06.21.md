### 使用vscode进行api的编译
```
脚本：使用dotnet命令
新建文件夹MyTest.Api 打开 
dotnet new sln 创建解决方案
创建webapi的两种方法：
1.先创建文件夹 mikdir  进入文件夹cd./ 然后去掉https协议dotnet new webapi --no-https
2.不提前创建文件夹 直接 创建同名项目文件夹 dotnet new webapi -n --no-https 

创建classlib类库：
mkdir Apitest.data  进入cd./
dotnet new classlib
安装一个nuget包：dotnet add package Newtonsoft.json
cd .. 回到总目录
dotnet sln add Mtapp.api 把解决方案添加到api

dotent add  Mtapp.api reference Apitest.data  引入api到Apitest.data类库
创建mkdir Api.Domian 进入cd./
dotent add my.api reference Apiout.Domian
然后把解决方案都加入类库：
dotnet sln add Apitest.data
dotnet sln add Api.Domian

这样基本框架就搭好了

dotnet restore 命令 还原项目包

``` 
常用命令
![](./imgs/999.png)           


#### 编写批处理文件一键生成框架
```
直接使用文本编辑：

改为文件后缀名.bat 管理员运行(PictureCore.bat)

:: 设置批处理文件所在目录为解决方案所在目录
cd /d %~dp0

:: 显示一些提示信息
@echo 请修改批处理文件名（以批处理的文件名作为解决方案的名称）
@pause
 
set name=%~n0
 
 ::创建解决方案的目录
mkdir %name%
cd %name%



:: 创建类库目录，进入类库目录，并使用dotnet命令创建类库项目（默认使用项目文件夹名称作为项目名称）
mkdir %name%.Data
cd %name%.Data
dotnet new classlib
dotnet add package log4net
dotnet add package Newtonsoft.Json
cd ..
 
mkdir %name%.Domain
cd %name%.Domain
dotnet new classlib
dotnet add package log4net
dotnet add package Newtonsoft.Json
cd ..
 
mkdir %name%.Api
cd %name%.Api
dotnet new webapi
dotnet add package log4net
dotnet add package Newtonsoft.Json
cd ..
dotnet add %name%.Api/%name%.Api.csproj reference %name%.Data/%name%.Data.csproj
dotnet add %name%.Api/%name%.Api.csproj reference %name%.Domain/%name%.Domain.csproj

::sln
dotnet new sln

 
dotnet sln %name%.sln add %name%.Api/%name%.Api.csproj --solution-folder 01Web
dotnet sln %name%.sln add %name%.Data/%name%.Data.csproj --solution-folder 02Business
dotnet sln %name%.sln add %name%.Domain/%name%.Domain.csproj --solution-folder 02Business

 
::编译解决方案
dotnet build
 
::end
 
@echo 构建完成，按任意键退出
@pause


```