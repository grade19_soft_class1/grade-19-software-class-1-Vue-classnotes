#### 路由
```
安装：vue add router
<!-- <router-link> 默认会被渲染成一个 `<a>` 标签 -->

css伪元素： ::marker(兼容浏览器86以上版本)
<style scoped>

li::marker {
  color: blue;
  font-size: 1.2em;
}

</style>

动态路由匹配（路由传入参数和获取）：
let router = new VueRouter({
  routes: [
    
    { path: '/user/:id', component: User }
  ]
})
 /user/... 都将映射到相同的路由
 现在一般不传?id=... 的模式 它应用于get的请求，不能post请求，有局限性


{{$router.params.id}}
{{$router.query.name}}

传参可以使用params和query两种方式。
使用params传参只能用name来引入路由，即push里面只能是name:’xxxx’,不能是path:’/xxx’,因为params只能用name来引入路由，如果这里写成了path，接收参数页面会是undefined！！！。
使用query传参使用path来引入路由。
params是路由的一部分,必须要在路由后面添加参数名。query是拼接在url后面的参数，没有也没关系。
二者还有点区别，直白的来说query相当于get请求，页面跳转的时候，可以在地址栏看到请求参数，而params相当于post请求，参数不会再地址栏中显示。

具体区别使用参照：https://www.cnblogs.com/wsm777/p/13629394.html


返回一个函数方法 ：
<template>
{{$router.params.id}}
{{poo}}
</template>

data(){
    return:{}
},
computed:{
    poo:function(){
        return this.$router.params.id
    }
}

```
#### 捕获所有路由或 404 Not found 路由
```


通配符 (*)
路由 { path: '*' } 通常用于客户端 404 错误,如果你使用了History 模式，请确保正确配置你的服务器
{
  // 会匹配所有路径
  path: '*'
}
{
  // 会匹配以 `/user-` 开头的任意路径
  path: '/user-*'
}

404路由一般放在最下面，现在好像人性化取消了覆盖









HTML5 History 模式:history 模式 URL 跳转而无须重新加载页面

官方配置看这： https://router.vuejs.org/zh/guide/essentials/history-mode.html#%E5%90%8E%E7%AB%AF%E9%85%8D%E7%BD%AE%E4%BE%8B%E5%AD%90


前端配置mian.js：
let  router = new VueRouter({
  mode: 'history',
  routes: [...]
})
后端配置：
如：
history模式下配置nginx

location / {
  try_files $uri $uri/ /index.html;
}




history模式下配置Node.js

 

const http = require('http')
const fs = require('fs')
const httpPort = 80

http.createServer((req, res) => {
  fs.readFile('index.htm', 'utf-8', (err, content) => {
    if (err) {
      console.log('We cannot open "index.htm" file.')
    }

    res.writeHead(200, {
      'Content-Type': 'text/html; charset=utf-8'
    })

    res.end(content)
  })
}).listen(httpPort, () => {
  console.log('Server listening on: http://localhost:%s', httpPort)
})


其实就是解决匹配不到路由时返回404的问题    


```
------------------------------------------
插入一条百科resful:

resful是一种网络应用程序的设计风格和开发方式，基于HTTP，可以使用XML格式定义或JSON格式定义

![](./imgs/9.png)

