#### 表单输入绑定
#### 关键词v-model
```
单文本：
input v-model="message" placeholder="请输入你的值">
<p>Message is: {{ message }}</p>

data:{message:[]}

多行文本：
<textarea v-model="message" placeholder="请输入内容"></textarea>
<p>value:{{message}}</p>

data:{message:[]}

复选框：
<input type="checkbox" id="jack" value="Jack" v-model="checkedNames">
<label for="jack">Jack</label>
<input type="checkbox" id="john" value="John" v-model="checkedNames">
<label for="john">John</label>
<input type="checkbox" id="mike" value="Mike" v-model="checkedNames">
<label for="mike">Mike</label>
<br>
<span>Checked names: {{ checkedNames }}</span>
  
  let app = new Vue({

            el: "#app",
            data: {
               
                checkedNames:[]
            }
        })

单选按钮： 
      
<input type="radio" id="one" value="One" v-model="picked">
<label for="one">One</label>
<input type="radio" id="two" value="Two" v-model="picked">
<label for="two">Two</label>
<p>Picked: {{ picked }}</p>

data:{picked:''}


选择框：
<select v-model="selected">
<option disabled value="">请选择</option>
<option>1</option>
<option>2</option>
<option>3</option>
</select>
<p>Selected: {{ selected }}</p>

data:{selected:''}


多选：（绑定到数组）
 <select v-model="selected">
            <option v-for="option in options" v-bind:value="option.value">
                {{ option.text }}
            </option>
        </select>
<span>Selected: {{ selected }}</span>

let app = new Vue({

            el: "#app",
            data: {
                 selected: '请选择',
                options: [
                    { text: 'One', value: '1' },
                    { text: 'Two', value: '2' },
                    { text: 'Three', value: '3' }
            ]
            }
        })

值绑定：对于单选按钮，复选框及选择框的选项，v-model 绑定的值通常是静态字符串 (对于复选框也可以是布尔值)；

修饰符：
.lazy//change 事件之后进行同步：
<input v-model.lazy="msg">

.number//自动将用户的输入值转为数值类型
<input v-model.number="age" type="number">

.trim//自动过滤用户输入的首尾空白字符
<input v-model.trim="msg">


```