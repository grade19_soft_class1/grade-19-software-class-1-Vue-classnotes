### Vue2.0复活
####　cdn
```
直接引入：
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

打开网址下载vue.js：https://cn.vuejs.org/v2/guide/installation.html
(开发版/生产版)
```
#### 声明式渲染
```
<div id="app">{{msa}}</div>

<script> 
let app = new Vue({
            el: "#app",
            data: {
                msg: "牛子哥"
                }
            })
</script>
```
#### 指令v-bind 
```
绑定元素:
<div id="app">
  <span v-bind:title="message">
    <center><h1>{{msg}}</h1> </center>
    </span>
  
</div>
var app = new Vue({
  el: '#app',
  data: {
    message:  new Date().toLocaleString()
  }
})

```
#### seen
```
 <span v-if="seen">现在你看到我了</span>
  <span v-else>你看不到我</span>

var app = new Vue({
  el: '#app',
  data: {
   seen:true
  }
})


在控制台输入 app.seen = false，你会发现之前显示的消息消失了，然后会出现else的内容

```
#### v-for
```
遍历数组：
<table>

    <tr v-for="men in menut">

    <td>{{men.id}}</td>
    <td>{{men.name}}</td>
    <td>{{men.path}}</td>
    
    </tr>
 </table>


<Script>
        var app = new Vue({
            el: '#app',
            data: {

                menut:
                    [
                        {
                            id: 1,
                            name: "啊牛",
                            path: "/111"
                        },
                        {
                            id: 2,
                            name: "啊毛",
                            path: "/222"
                        },
                        {
                            id: 3,
                            name: "啊牛木",
                            path: "/333"
                        }],
            }
        })
</Script>

```
#### v-on 
```
处理用户输入：



<table>
        <tr v-for="men in menut">

        <td>{{men.id}}</td>
        <td>{{men.name}}</td>
 </tr>
</table>
<button v-on:click="say">哈罗</button>


let app = new Vue({
            el: "#app",
           

            data: {
                menut:
                    [
                        {
                            id: 1,
                            name: "牛牛",
                          
                        }

                    ],
            }, 
            methods: {
                say: function () {
                    this.menut.push({
                        id: 666,
                        name: "牛牛",
                        
                    })
                }
            }
})



```
####　v-model
```
实现表单输入和应用状态之间的双向绑定:
<form >
          <label for="">账号：</label><input type="text" v-model="aa.username">
          <label for="">密码：</label><input type="password" v-model="aa.password">
</form>
<p v-if="seen">{{str}}</p>  //不打印信息，seen为true打印

<script>
   
        let app = new Vue({
            el: "#app",
            data: {
               seen:false,
                
                aa: {
                    username: 'admin',
                    password: '113'

                },
               
                


                menut:
                    [
                        
                    ]
            },
            
            computed: {
                str: function () {
                    return `账号:${this.aa.username},密码：${this.aa.password}`
                }
            }
        })
    </script>




```
#### 注册组件todo-item
```
  <ul>
    <todo-item v-for="item in listaa" v-bind:todo="item" v-bind:key="item.id">

   </todo-item>
  </ul> 


<script>
   Vue.component('todo-item', {
  // todo-item 组件现在接受一个
  // "prop"，类似于一个自定义 attribute。
  // 这个 prop 名为 todo。
  props: ['todo'],
  template: '<li>{{ todo.name}}</li>'
})

 let app = new Vue({
            el: "#app",
            data: 
            {
                 listaa:[
                    {   id:1,
                        name: "111"
                    },
                    {   id:2,
                        name: "222"
                    },
                    {   id:3,
                        name: "333"
                    },

                ]
            }
 })

 </script>

```