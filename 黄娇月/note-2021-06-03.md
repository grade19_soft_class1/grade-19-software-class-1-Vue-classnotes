### 事件处理方法
🤷 用 v-on:click.prevent.self 会阻止所有的点击。  
🤷  v-on:click.self.prevent 只会阻止对元素自身的点击。
```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="app">
        <div v-on:click.self="doThis(2)">
            <input type="text" value="薄荷清香" v-model="msg" placeholder="修身养性">
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script> -->
    <script>
        let app = new Vue({
            el: '#app',
            data: {
                lists: [
                    {

                    }
                ],
                msg: ' 玫瑰美容养颜'
            },
            methods: {
                btnClick: function (id, event) {
                    console.log(id);
                    console.log(event);
                },
                doThis: function (id) {
                    console.log((id));
                }
            }

        })
    </script>
</body>

</html>
```
👉效果如图所示：  
![薄荷](./imgs/2021-06-03_111717.png)
![玫瑰](./imgs/2021-06-03_111703.png)


### 监听事件
🤷 用 v-on 指令监听 DOM 事件。  
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <div v-on:click.capture="doThis"></div>
        <input type="button" @click="btnClick(1,$event)" value="薄荷清香">
        
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script> -->
    <script>
        let app=new Vue({
            el:'#app',
            data:{
                lists:[
                    {

                    }
                ]
            },
            methods:{
                btnClick:function(id,event){
                    console.log(id);
                    console.log(event);
                }
            }
        
        })
    </script>
</body>
</html>
```
👉效果如图所示：  
![薄荷](./imgs/2021-06-03_101913.png)

### 事件修饰符

 🤷 阻止单击事件继续传播     
 ```
<a v-on:click.stop="doThis"></a>
```

🤷🤷  提交事件不再重载页面   
```
<form v-on:submit.prevent="onSubmit"></form>
```


 🤷🤷🤷  修饰符可以串联   
 ```
<a v-on:click.stop.prevent="doThat"></a>
```


🤷🤷🤷🤷  只有修饰符  
```
<form v-on:submit.prevent></form>
```


🤷🤷🤷🤷🤷 添加事件监听器时使用事件捕获模式  
🤷🤷🤷🤷🤷 即内部元素触发的事件先在此处理，然后才交由内部元素进行处理  
```
<div v-on:click.capture="doThis">...</div>
```


🤷🤷🤷🤷🤷🤷  只当在 event.target 是当前元素自身时触发处理函数  
🤷🤷🤷🤷🤷🤷  即事件不是从内部元素触发的  
```
<div v-on:click.self="doThat">...</div>
```



