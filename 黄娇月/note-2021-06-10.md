### Windows PowerShell 安装Vue命令
✨ npm config set registry https://registry.npm.taobao.org (设置淘宝镜像，下载的速度会比较快)   
✨ npm i -g yarn (安装)   
✨ yarn global add @vue/cli (yarn 安装 vue-cli)  
✨ vue  

👉输入vue出现错误如图所示：     
![出现错误](./imgs/2021-06-11_151152.png)

👱‍♀️ 解决方法步骤如下：  
```    
设置高级变量：我的电脑️ ➡ 属性 ➡ 高级系统设置️ ➡ 环境变量️ ➡ 都选择️Path ➡ 编辑️ ➡ 新建️ ➡ 浏览️C盘️ ➡用户️➡ Administator️ ➡ AppData️ ➡ Local️ ➡ Yarn️ ➡ bin️ ➡ 确认   
``` 
👉如图所示：  
![](./imgs/2021-06-11_173453.png)

👱‍♀️ 配置完成后就可以使用vue-cli了   
👱‍♀️👱‍♀️ 重新打开 Windows PowerShell 输入 vue 即可👇              
![输入vue](./imgs/2021-06-11_153031.png)

✨ d:  
✨ mkdir myProjects  (创建一个空目录)   
✨ vue create my-vue-app (创建vue项目)   
✨ yes      
✨ Default ([Vue 2] babel, eslint) (按空格或↑↓进行选择，按回车进行下一步)     
✨ Use Yarn (按空格或↑↓进行选择，按回车进行下一步)      
✨ vue ui (命令会打开一个浏览器窗口，并以图形化界面将你引导至项目创建的流程)     

👉输入vue ui后如图所示：   
![安装成功](./imgs/2021-06-11_160938.png)


### 布局命令
👱‍♀️   
✨ cd  
✨ npm -v  
✨ yarn -v  
✨ yarn config set registry https://registry.npm.taobao.org  
✨ yarn global add @vue/cli  
✨ ll  
✨ vue  
✨ vue create my-hello-world  



