### 什么是Vue CLl
🙆‍♂️  CLI (@vue/cli) 是一个全局安装的 npm 包，提供了终端里的 vue 命令。  
🙆‍♂️  可以通过 vue create 快速搭建一个新项目，或者直接通过 vue serve 构建新想法的原型。  
🙆‍♂️ 可以通过 vue ui 通过一套图形化界面管理你的所有项目。  

### Vue CLl安装
🌻 可以使用下列任一命令安装这个新的包：  
```
npm install -g @vue/cli
```
🌻 OR
```
yarn global add @vue/cli
```
🌻 安装之后 (可以用这个命令来检查其版本是否正确) 
```
vue --version
```
### 升级
🌻 需升级全局的 Vue CLI 包，请运行:  
```
npm update -g @vue/cli
```
🌻 或者
```
yarn global upgrade --latest @vue/cli
```

