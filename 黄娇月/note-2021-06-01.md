### 列表渲染
✨  列表渲染主要是把数组的数据渲染到指定的地方，简化代码工作量。 

### 在组件上使用 v-for 
① 用 v-for 来遍历一个对象的 property。  
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>列表渲染t</title>
</head>
<body>
    <div id="app">
        <ul>
            <li v-for="(item,a) in lists">{{a+1}}-{{item}}</li>
        </ul>

    </div>
    
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script>
        let app=new Vue({
            el:'#app',
            data:{
                lists:{
                    title:'山河令',
                    author:'风起霓裳',
                    timeAt:'2021-05-29'
                }
            }
        })
    </script>
</body>
</html>
```
👉效果如图所示：  
![效果如图所示](./imgs/2021-06-01_163335.png)

② v-for 指令可以绑定数组的数据来渲染一个项目列表：
```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div id="app-4">
        <ol>
            <li v-for="tois in tois">
                {{tois.text}}

            </li>
        </ol>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
        let app4 = new Vue({
            el: '#app-4',
            data: {
                tois: [
                    { text: '琉璃' },
                    { text: '双世宠妃' },
                    { text: '我想和你在一起' }
                ]


            }
        })
    </script>

</body>

</html>
```
👉效果如图所示：  
![电视剧](./imgs/2021-06-04_153822.png)

🤺 在控制台，输入 app4.tois.push({ text: '唐人街探案3' })后
![变化](./imgs/2021-06-04_154055.png)


③ v-for就是渲染的指令，需要使用-item in items- 形式的特殊语法，items 是源数据数组，item 是数组元素迭代的别名。  
```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>列表渲染t</title>
</head>
<body>
    <div id="app">
        <form action="">
            <input type="text" v-model="newItem">
            <input type="button" value="添加" @click="addToLists">
        </form>

        <ul>
            <do-item v-for="(item,index) in lists" :key="item.id" :title="item.todoItem" @remove="lists.splice(index,1)"></do-item>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

    <script>
        Vue.component('do-item',{
            props:['title'],
            template:`<li>{{title}}<input type="button" value="移除" @click="$emit('remove')"></li>`
        })

        let app =new Vue({
            el:'#app',
            data:{
                newItem:'',
                lists:[
                    {
                        id:1,
                        todoItem:'无骨鱼(●ˇ∀ˇ●)',
                    },
                    {
                        id:2,
                        todoItem:'无骨鸡爪(❤ ω ❤)',
                    },
                    {
                        id:3,
                        todoItem:'酸菜鱼(●ˇ∀ˇ●)',
                    },
                ]
            },
            methods:{
                addToLists:function(){
                    console.log(this.newItem);
                    this.lists.push({
                        id:this.lists.length+1,
                        todoItem:this.newItem
                    })
                    this.newItem=''
                }
            }
        })
    </script>
</body>

</html>
```
👉效果如图所示：   
![效果如图所示](./imgs/微信截图_20210602160200.png)

✨ Vue数组变更方法  
 :tw-2744: pop()和shift()方法不接受传参，传参了也没有什么用。  
 :tw-2744: 空数组可以删除，不报错，但返回undefined。    
```
var list = [3,4,5,6]
```

|  push()   |   向数组的尾部添加若干元素，并返回数组的新长度 |
|---|---|
| pop()  | 从数组的尾部删除一个元素（删且只删除一个元素），返回被删除的元素  |
|  unshift()  |  从数组的头部删除一个元素（删且只删除一个元素），返回被删除的元素 |
|    sort() | 用于对数组的元素进行排序  |
|  reverse()  |  用于颠倒数组中元素的顺序 |
