# Jwt以及token生成、验证
[jwt-asp.net-core](https://www.cnblogs.com/7tiny/archive/2019/06/13/11012035.html)

[详解跨域](https://www.imooc.com/article/21976)

[Dotnet-core使用JWT认证授权最佳实践](https://www.cnblogs.com/tiger-wang/p/12892383.html)

    安装扩展
    将文件克隆后需要dotnet restore
    cd .\Slight.Backend.Api\
    dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
    显示框架不兼容，运行下列
    dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer -h
    dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer -v 3.1

    于appsettings.json添加：
    "tokenParameter":{
        "secret":"十六位秘钥",
        "issuer":"WangPlus",
        "accessExpiration":120，
        "refreshExpiration":1440
    }

    于Params添加TokenParameter.cs文件：
    namespace Slight.Backend.Api.Params
    {
        public class TokenParameter
        {
            //生成token的所需要的密钥，一定不能泄露
            public string Secret {get;set;}
            //发行token的发行人(可以是个人或者组织)
            public string Issuer {get;set;}
            //token的有效分钟数
            public int AccessExpiration {get;set;}
            //RefreshExpiration有效分钟数
            public int RefreshExpiration {get;set;}
        }
    }
## 惟愿忧心之时能有颗糖甜入心