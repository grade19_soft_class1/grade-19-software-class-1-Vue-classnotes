# 不论在何时何地都要控制好情绪
## Class与Style绑定 
    操作元素的class列表和内联样式是数据绑定的一个常见需求.
    将v-bind用于class和style时，Vue.js做了专门增强.
    表达式结果的类型除了字符串之外，还可以是对象或数组.
### 绑定HTML Class
    可以传给v-bind:class一个对象，以动态地切换class:
    <div v-bind:class="{active:isActive}"></div>
    active这个class存在与否将取决于数据property isActive的truthiness.
    可以在对象中传入更多字段来动态切换多个class.
    <div 
        class="static"
        v-bind:class="{active:isActive,'text-danger':hasError}"    
    ></div>
    和如下data:
    data:{
        isActive:true,
        hasError:false
    }
    渲染，当isActive或者hasError变化时，class列表将相应地更新.
![Vue](./Vue/vue21.PNG)
### 直接一波了(数组语法、用在组件、绑定内联样式)
![Vue](./Vue/vue22.PNG)

    使用if有小概率会使背景颜色和文字颜色重合，使用while则不会
## 老胡安利
[新时代创意布局不完全指南](https://www.cnblogs.com/coco1s/p/14816871.html)

[不知道说什么了，很🐂很炫](https://github.com/chokcoco/iCSS)