# 那就忘了吧，没什么可留恋的
## Asp.Net-Core审计日志
[.Net Core审计日志实现](https://www.cnblogs.com/cwsheng/p/14199085.html)

    审计日志定义："审计跟踪(审计日志)是与安全相关的按照时间顺序的记录，记录集或记录源，它们提供了活动序列的文档证据，这些活动序列可以在任何时间影响一个特定的操作，步骤或其它".

    作用：1.快速定位问题耗时及时性能情况
    2.记录调用时环境信息：如浏览器、参数等

    实现审计日志的核心思想包含以下步骤：
    1.获取调用接口方法时相关信息
    2.记录当前接口耗时情况
    3.保存审计日志信息到数据库中

    获取调用接口时相关信息：过滤器、拦截器实现

![Asp.Net-Core](./Vue/aspnetcore.png)

    大部分情况下，不将逻辑写入UsersController.cs文件，而是写入Service.cs文件

    在Entity创建AuditInfo.cs文件定义审计日志信息
    新建Filters文件夹含AuditLogActionFilter，实现审计日志过滤器
    于Startup.cs文件完成注册过滤器

    安装扩展
    克隆下来dotnet restore
    dotnet build查找错误，进行修改
    cd 文件夹
    dotnet tool install --global dotnet-ef 
    dotnet ef migrations add 实现审计日志实体类
    dotnet ef database update
    dotnet run运行后在数据库中显示审计日志记录

    在.net core中需要统一监控或过滤时，可以采用过滤器(Filter)或拦截器来实现相关效果
    .Net Core中Filter常见的有：
    1.Authorization Filter（认证过滤器）
    2.Resource Filter（资源过滤器）
    3.Exception Filter（异常过滤器）
    4.Action Filter（方法过滤器）
    5.Result Filter（结果过滤器）

    问题：实现token功能，判断有无用户登录，生成一组种子数据，不至于使用时需要注释掉
[asp.net-core添加种子数据](https://blog.csdn.net/qq_27848323/article/details/107639804)

## 理清思路真的很重要呱