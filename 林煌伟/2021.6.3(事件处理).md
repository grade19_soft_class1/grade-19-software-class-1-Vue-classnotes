## 2021.6.3

# 内容

## 事件处理

``` 监听事件 ```
```
    可以通过v-on指令来监听DOM事件，并触发运行JavaScript语法

        <input type="button" value="加一" @click="msg += 1">
        <p>{{msg}}</p>

    上面的@click 是缩写 v-on:click，渲染的效果就是点击加一按钮下面的msg就会加数字一
    如果msg是字符串的话，这个方法会将一自动转换成字符串添加到msg后面
```

``` 事件处理方法 ```
```
    v-on还可以调用方法

        <input type="button" value="加一" @click="com">
        
            methods:{
                com:function(){
                    return this.msg+=2
                }
            }
    
    在调用方法时还可以给在方法中传入参数

            methods:{
                com:function(event){
                    alert(this.msg)
                     this.msg+=2
                    console.log(event);
                }
            }
    
    其中event是原生DOM事件

        <input type="button" value="加一" @click="com(5)">

            methods:{
                com:function(message){
                    alert(message)
                }
            }

    点击加一按钮会出现一个内容为5的弹窗
```

``` 事件修饰符 ```
```
    .prevent修饰符的效果是提交事件时不会重新加载页面
        <form action="">
            <p>{{msg}}</p>
            <button @click.prevent="f">按钮</button>
        </form>
    
    .stop的效果是阻止单机事件的传播

        <div @click="dTest">
            <input type="button" value="test" @click.stop="test">
        </div>

    在没有使用stop修饰符时会有两个弹窗，使用stop修饰符时div中的弹窗被阻止了

    .captrue修饰符效果是先处理当前元素，然后在处理内容元素

        <div @click.capture="dTest">
            <input type="button" value="test" @click.stop="test">
        </div>
    
    使用时会先弹出弹出div中的元素然后在弹出input中的元素

    .once修饰符效果是只会触发一次事件

        <input type="button" value="加一" @click.once="com">

    使用.once后只能加一次一，后续点击没有效果
```

``` 按键修饰符 ```
```
    .enter
    回车键

    .tab
    tab键

    .delete 
    删除和退格键
    .esc
    
    .space
    空格键

    .up
    .down
    .left
    .right

    方向键
```
