#  Dotnet  Get Post Put Delete 用法

## 本次笔记是在前一日笔记的基础上实现，如不懂请从上一篇笔记开始

### 先安装Rest Client 插件 

### 随后在Controllers 文件夹下 创建你需要的以.cs后缀的文件名，我这边创建的是UsersController.cs（大多数代码可以参考自动生成的另一位.cs结尾的文件）
```
代码如下：

using Microsoft.AspNetCore.Mvc;

namespace MyTest.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    
    public class UsersController:  ControllerBase
    {
        public string  Get()
        {
            return "666";
        }
    }
}
```

### 还需创建一个以.http 为后缀的文件名 我创建的是ihateyou.http
```
代码如下：
###
GET http://localhost:5000/WeatherForecast

###
GET http://localhost:5000/users 
```
### 当我们点击两个的链接时，跳出的页面是这样子的
### 点击第一个链接时：
![图片](imgs/2021-06-22.1.jpg)

### 点击第二个链接时：

![图片](imgs/2021-06-22.2.jpg)


# Get 操作！
### 创建一个新的文件夹 Entity 在该文件夹下在创一个Users.cs
```
Users.cs：
定义一些属性
namespace ApiTest.Api.Entity
{
    public class Users
    {
        public int Id{get;set;}
        public string Username {get;set;}
        public string Password {get;set;}
    }
}
```
UsersController:
```
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ApiTest.Api.Entity;

namespace MyTest.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    
    public class UsersController:  ControllerBase
    {
       public  IEnumerable<Users> Get()
       {
           var users=new List<Users>
           {
               new Users
               {
                   Id=1,
                   Username="阮",
                   Password="123456"
               },
               new Users
               {
                   Id=2,
                   Username="熊",
                   Password="123456"
               },
               new Users
               {
                   Id=3,
                   Username="朱",
                   Password="123456"
               },
               new Users
               {
                   Id=4,
                   Username="鲁",
                   Password="123456"
               },
           };
           return users;
           
       }
       [HttpGet("{id}")]
        public string Get(int id)
        {
            return string.Format("id是：{0}",id);
        }
    }
        
}
```
效果如下：
![图片](./imgs/2021-06-22.3.jpg)

# Post 请求

### 在UsersController.cs文件中继续写代码
```
写这个：

   [HttpPost]
        public dynamic Post(dynamic model)
        {
            return new
            {
                Code=200,
                Data=model,
                Msg="请求成功"
            };
        }
```

### 在i.http 文件下写代码：
```
 写这个：

###
POST http://localhost:5000/users
Content-Type: application/json

{
    "username" : "爱学习的熊耀阳",
    "password" : "123"
}
```

效果如下：![图片](./imgs/2021-06-22.4.jpg)


# Put 请求~

### 在UsersController.cs文件中继续写代码
```
写这个：

[HttpPut("{id}")]
        public dynamic Put(int id)
        {
            return new
            {
                Code=200,
                Data=string.Format("id是：{0}",id),
                Msg="修改成功"
            };
        }
```

### 在 i.http 写这个
```
###

PUT http://localhost:5000/users/3
```

运行后，效果如下：
![图片](./imgs/2021-06-22.5.jpg)


# Delete 请求！

### 在UsersController.cs文件中继续写代码
```
你懂得，接着写：

[HttpDelete("{id}")]
        public dynamic Delete(int id)
        {
            return new
            {
                Code=200,
                Data=string.Format("id就是你觉得的这个：{0}",id),
                Msg="删除成功，准备跑路"
            };
        }

```

### 在i.http 写
```
###
DELETE  http://localhost:5000/users/3
```

运行后，效果如下：
![图片](./imgs/2021-06-22.6.jpg)