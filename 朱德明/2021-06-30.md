# 针对实体对象的CRUD，定义一个相应的接口

### 创建一个文件夹Repository 创IRepository.cs文件
```
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using MyTest.Api.Entity;

namespace MyTest.Api.Repository
{
    /// <summary>
    ///  定义接口，用于实体CRUD操作
    /// </summary>
    /// <typeparam name="T">泛型，可以是任意一个实体类型</typeparam>
    public interface IRespotitory<T> where T:BaseEntity
    {
        /// <summary>
        /// 一个属性，是一个可查询的表
        /// </summary>
        /// <value></value>
        /// 
         IQueryable<T> Table{get;}
        /// <summary>
        /// 根据id来查询对应的T类型实例
        /// </summary>
        /// <param name="id">id在这里，可能是int，long，guid和一些常见的主键类型</param>
        /// <returns></returns>
        /// 
        T GetById(object id);

        /// <summary>
        /// 根据提供的实体对象，插入数据库
        /// </summary>
        /// <param name="entity">提供的实体对象</param>
         void Insert(T entity);

        /// <summary>
        /// 根据提供的实体对象合集，插入到数据库（异步）
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
         Task InsertAsync(T entity);

        /// <summary>
        /// 根据提供的实体对象合集，批量插入到数据库
        /// </summary>
        /// <param name="entities"></param>
         void InsertBulk(IEnumerable<T> entities);

         /// <summary>
         /// 根据提供的实体对象合集，插入到数据库（异步）
         /// </summary>
         /// <param name="entities"></param>
         /// <returns></returns>
         Task InsertBulkAsync(IEnumerable<T> entities);

        /// <summary>
        /// 根据提供的主键Id,删除数据库的记录
        /// </summary>
        /// <param name="id"></param>
         void Delete(object id);

         /// <summary>
         /// 根据提供的主键Id集合,批量删除数据库的记录
         /// </summary>
         /// <param name="ids">提供的Id 的集合</param>
         void DeleteBulk(IEnumerable<int> ids);

        /// <summary>
        /// 根据提供的实体对象，更新到数据库
        /// </summary>
        /// <param name="entity">实体对象</param>
        void Update(T entity);

        /// <summary> 
        /// 根据提供的实体对象，批量更新到数据库
        /// </summary>
        /// <param name="entities">实体对象合集</param>
        void UpdateBulk(IEnumerable<T> entities);
    }
}
```



## 定义一个类型来实现IRepository接口
### 在Repository 创建一个EfRepository.cs文件代码如下：（大部分接口是由vs一键生成的哈哈哈）
```
using MyTest.Api.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyTest.Api.Database;
using Microsoft.EntityFrameworkCore;
using System;

namespace MyTest.Api.Repository
{
    public class EfRepotitory<T> : IRepository<T> where T : BaseEntity
    {
        private MyTestDb _db=new MyTestDb();

        private DbSet<T> _entity;

        protected DbSet<T> Entity{
            get
            {
            if(_entity ==null){
                _entity=_db.Set<T>();
            }
            return _entity;
            }
        }
        public IQueryable<T> Table
        {
            get
            {
            return Entity.AsQueryable<T>();
            }
        }

        public void Delete(object id)
        {
            var t =Entity.Where(x=>x.Id==(int)id).FirstOrDefault();
            _db.Remove(t);
            _db.SaveChanges();
        }

                public void DeleteBulk(IEnumerable<object> ids)
        {
            var ii = new List<int>();
            foreach (var item in ids)
            {
                var tmp = (int)item;
                ii.Add(tmp);
            }
            var ts = Entity.Where(x => ii.Contains(x.Id)).ToList();
            _db.RemoveRange(ts);
            _db.SaveChanges();
        }

        public T GetById(object id)
        {
            return Entity.Find(id);
        }

        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            entity.DisplayOrder = 0;

            Entity.Add(entity);
            _db.SaveChanges();
        }

        public async Task InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;
            entity.DisplayOrder = 0;

            await Entity.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public void InsertBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
                entity.DisplayOrder = 0;
            }
            Entity.AddRange(entities);
            _db.SaveChanges();
        }

        public async Task InsertBulkAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
                entity.DisplayOrder = 0;
            }
            await Entity.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            entity.UpdatedTime = DateTime.Now;
            _db.SaveChanges();
        }

        public void UpdateBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.UpdatedTime = DateTime.Now;
            }

            Entity.UpdateRange(entities);
            _db.SaveChanges();
        }

    }
}
```

## 在Controllers下创建UsersController 文件
```
1
```
