# 表单输入绑定

你可以用 v-model 指令在表单input、textarea 及 select 元素上创建双向数据绑定。它会根据控件类型自动选取正确的方法来更新元素。尽管有些神奇，但 v-model 本质上不过是语法糖。它负责监听用户的输入事件以更新数据，并对一些极端场景进行一些特殊处理。

例如： 文本 和 多行文本
```
  <div id="app">
        <h3>单行文本：</h3>
        <br>
        <input v-model="message" placeholder="输入你的账号">
        <p>输入的账号是：{{message}}</p>
        <hr>
        <h3>多行文本：</h3>
        <br>
        <span>输入你想输入的东西：</span>
        <p style="white-space:pre-line;">{{message2}}</p>
        <br>
        <textarea v-model="message2" placeholder="这是你输入的内容"></textarea>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.13"></script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: '',
                message2: ''
            }
        })
    </script>
```
如下：
![图片](./imgs/2021-06-07-1.jpg)

###  复选框  单选按钮  选择框
```
 <div id="app">

        <h3>复选框</h3>


        <br>
        <input type="checkbox" id="checkbox" v-model="checked">
        <label for="checkbox">{{ checked }}</label>


        <h3>多个复选框绑定一起</h3>


        <input type="checkbox" id="jack" value="早上了" v-model="checkedNames">
        <label for="1">早上了</label>
        <input type="checkbox" id="john" value="中午了" v-model="checkedNames">
        <label for="2">中午了</label>
        <input type="checkbox" id="mike" value="晚上了" v-model="checkedNames">
        <label for="3">晚上了</label>
        <br>
        <span>现在是什么时候: {{ checkedNames }}</span>


        <h3>单选按钮</h3>


        <br>
        <input type="radio" id="one" value="正确" v-model="picked">
        <label for="one">正确</label>
        <br>
        <input type="radio" id="two" value="错误" v-model="picked">
        <label for="two">错误</label>
        <br>
        <span>你的选择是: {{ picked }}</span>


        <h3>选择框</h3>


        <br>
        你认为你是傻瓜吗？
        <br>

        <select v-model="selected">
            <option disabled value="">请选择</option>
            <option>是的</option>
            <option>没错</option>
            <option>的确</option>
          </select>
          <span>你的选择是: {{ selected }}</span>
      
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.13"></script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {

                checked: '',
                checkedNames: [],
                picked:'',
                selected:''
            }
        }) 
    </script>
```
如下：
![图片](./imgs/2021-06-07-2.jpg)
