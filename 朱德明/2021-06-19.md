# 嵌套路由
事例
### components下文件  bad good  User .vue
```
bad.vue:
<template>
   <div>
       <hr>
       阮的缺点
       <ul>
           <li>今天天气好晴朗</li>
           <li>处处好</li>
           <li>你好</li>
           <li>我好</li>
           <li>大家好</li>
        
       </ul>
   </div>
</template>


good.vue:

<template>
    <div>
        <hr>
        阮的简历
        <table>
            <tr>
                <td>姓名</td>
                <td>年龄</td>
                <td>性别</td>
                <td>是否婚配</td>
            </tr>
            <tr>
                <td>阮铲冰</td>
                <td>21</td>
                <td>man</td>
                <td>是</td>
            </tr>

        </table>
       <router-view></router-view>
    </div>
</template>

User.vue:

<template>
    <div>
        <h2>用户表</h2>
        <router-link to="bad"></router-link>
        <router-link to="good"></router-link>
       阮的小秘密
    <br>
       <a href="/usergood">好的</a>
       <br>
       <a href="bad">坏的</a>
        <router-view></router-view>
    </div>
</template>
```
### main.js 
```
import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import User from './components/User';
import good from './components/good';
import bad from './components/bad';
import friends from './components/friends';

Vue.config.productionTip = false

Vue.use(VueRouter)

let router =new VueRouter({
   mode:'history',
   routes:[
     {
     path:'/user',
     component:User,
     children:[
      {
        path:'good',
        component:good,
        children:[
          {
          path:'friend',
          component:friends
          }
        ]
        },
        {
          path:'bad',
          component:bad,
          },
     ]
     },
    
   ]
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

```
### 效果如下： 先进入user，然后点击如图的按钮，就会出现不同的界面 这就是嵌套路由！
这是进入User界面的效果：
<br>
![图片](./imgs/2021-06-19.1.jpg)
<br>
这是点击好的按钮进入的界面：
<br>
![图片](./imgs/2021-06-19.2.jpg)
<br>
这是进入坏的的按钮的界面：
<br>
![图片](./imgs/2021-06-19.3.jpg)